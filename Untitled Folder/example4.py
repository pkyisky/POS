from PyQt4 import QtGui, QtCore
import sys

# class MyApp(QtGui.QMainWindow):
#
#     def __init__(self, *args):
#         super(MyApp, self).__init__(*args)
#         self.scrollArea = QtGui.QScrollArea()
#
#         self.initUI()
#
#     def initUI(self):
#         self.vbox = QtGui.QVBoxLayout()
#         self.push = QtGui.QPushButton()
#         # self.vbox.addWidget(self.push)
#
#         highlight_dir = '/home/cioc/Documents/pos/images'
#         content_widget = QtGui.QWidget()
#         self.scrollArea.setWidget(content_widget)
#         self._layout = QtGui.QGridLayout(content_widget)
#         self._it = QtCore.QDirIterator(highlight_dir)
#         self.grid = QtGui.QGridLayout()
#         self.vbox = QtGui.QVBoxLayout()
#
#         self.mainLayout = QtGui.QGridLayout()
#         self.mainLayout.addLayout(self.vbox,0,1)
#         self.mainLayout.addWidget(self.scrollArea,0,0)
#         self.setCentralWidget(QtGui.QWidget(self))
#         self.centralWidget().setLayout(self.mainLayout)
#
#
#
#
#
#
# if __name__ == '__main__':
#     import sys
#     app = QtGui.QApplication(sys.argv)
#     w = MyApp()
#     w.setGeometry(500, 300, 500, 500)
#     w.show()
#     sys.exit(app.exec_())
from PyQt4 import QtGui, QtCore
import sys

class test(QtGui.QWidget):

    def __init__(self,parent=None):

        self.widget=QtGui.QWidget.__init__(self, parent)

         # Button to add labels
        self.btnAdd = QtGui.QPushButton('Add')
        self.btnAdd.connect(self.btnAdd, QtCore.SIGNAL('clicked()'),self.btnAddPressed)

        # Button to remove labels
        self.btnRemove = QtGui.QPushButton('Remove')
        self.btnRemove.connect(self.btnRemove, QtCore.SIGNAL('clicked()'), self.btnRemovePressed)

        # List to keep track of labels
        self.labels=[]
        print self.labels, "labels"

        # Layout
        self.hbox = QtGui.QHBoxLayout()
        self.hbox.addWidget(self.btnAdd)
        self.hbox.addWidget(self.btnRemove)
        self.setLayout(self.hbox)


        self.show()

    def btnAddPressed(self):
        self.labels.append(QtGui.QLabel("lbl"+str(len(self.labels)+1), self))
        print str(len(self.labels)+1)
        print self.labels[-1]
        self.hbox.addWidget(self.labels[-1])

    def btnRemovePressed(self):
        self.labels[-1].setParent(None)
        self.labels.pop(-1)


def main():

    #Creating application
    app = QtGui.QApplication(sys.argv)

    main_win = test()
    sys.exit(app.exec_())

if __name__ == '__main__':
     main()

import sys
from functools import partial
from PyQt4 import QtGui, QtCore

# from simple_table import Settings

ROUNDED_STYLE_SHEET1 = """QPushButton {
     background-color: yellow;
     color: brown;
     border-style: outset;
     border-width: 4px
     border-radius: 15px;
     border-color: blue;
     font: bold 17px;
     min-width: 7em;
     padding: 8px;
 }
"""
ROUNDED_STYLE_SHEET2 = """QPushButton {
     background-color: green;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 12px;
     min-width: 25em;
     padding: 25px;
 }
"""

ROUNDED_STYLE_SHEET3 = """QPushButton {
     background-color: orange;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 12px;
     min-width: 25em;
     padding: 25px;
 }
"""
ROUNDED_STYLE_SHEET4 = """QPushButton {
     background-color: blue;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 12px;
     min-width: 25em;
     padding: 25px;
 }
"""
ROUNDED_STYLE_SHEET5 = """QPushButton {
     background-color: blue;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 20px;
     min-width: 8em;
     padding: 8px;
 }
"""
ROUNDED_STYLE_SHEET6 = """QPushButton {
     background-color: red;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: blue;
     font: bold 17px;
     min-width: 8em;
     padding: 10px;
 }
"""
ROUNDED_STYLE_SHEET7 = """QPushButton {
     background-color: brown;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: red;
     font: bold 17px;
     min-width: 10em;
     padding: 18px;
 }
"""
OVAL = """QPushButton {

      position: relative;
      width: 100px;
      height: 70px;
      margin: 20px 0;
      background: green;
      border-radius: 48% / 25%;
      color: white;
      border-color: blue;
      font: bold 20px;
}
"""
QSS = '''
QLabel#big{
    font: bold 20pt Comic Sans MS
}
QLabel#small{
    font: bold 15pt Comic Sans MS
}
'''

class Example1(QtGui.QWidget):
    def __init__(self, parent=None):
        super(Example1, self).__init__(parent)
        self.initUI()

    def initUI(self):
        grid = QtGui.QGridLayout(self)
        label_payment = QtGui.QLabel("Payment Details", objectName="big")
        label_received = QtGui.QLabel("Received Amount", objectName="big")

        label_amount = QtGui.QLabel("Amount: 192", objectName="small")
        label_quantity = QtGui.QLabel("Quantity: 1", objectName="small")

        cash_button = QtGui.QPushButton("Cash")
        card_button = QtGui.QPushButton("Card")
        wallet_button = QtGui.QPushButton("Wallet")

        lcd = QtGui.QLCDNumber()
        sp = lcd.sizePolicy()
        sp.setHorizontalPolicy(QtGui.QSizePolicy.Fixed)
        lcd.setSizePolicy(sp)

        grid_buttons = QtGui.QGridLayout()

        names = ['7', '8', '9',
                 '4', '5', '6',
                '1', '2', '3',
                 '<--', '0', '.']
        positions = [(i,j) for i in range(4) for j in range(3)]
        for position, name in zip(positions, names):
            button = QtGui.QPushButton(name)
            grid_buttons.addWidget(button, *position)

        grid.addWidget(label_payment, 0, 0, 2, 3)
        grid.addWidget(label_amount, 0, 5)
        grid.addWidget(label_quantity, 1, 5)

        hbox = QtGui.QHBoxLayout(spacing=0)
        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.addWidget(cash_button)
        hbox.addWidget(card_button)
        hbox.addWidget(wallet_button)
        grid.addLayout(hbox, 3, 0, 1, 6)

        vbox = QtGui.QVBoxLayout(spacing=0)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(label_received)
        vbox.addWidget(lcd)
        grid.addLayout(vbox, 4, 1)

        grid.addLayout(grid_buttons, 4, 3, 4, 3)
        self.setFixedSize(self.sizeHint())

class Example(QtGui.QWidget):

    def __init__(self):
        super(Example, self).__init__()

        self.initUI()

    def initUI(self):
        self.grid = QtGui.QGridLayout()
        self.hbox1 = QtGui.QHBoxLayout()
        self.label = QtGui.QLabel("Invoice Serial Number-39")
        self.label.setStyleSheet("font: bold 20pt AGENTORANGE")
        self.hbox1.addWidget(self.label)
        self.hbox1.addStretch()
        self.lineedit = QtGui.QLineEdit()
        self.lineedit.resize(250, 40)
        self.hbox1.addWidget(self.lineedit)

        self.connectbtn = QtGui.QPushButton("Connect")
        self.connectbtn.setStyleSheet(ROUNDED_STYLE_SHEET1)
        self.hbox1.addWidget(self.connectbtn)
        self.grid.addLayout(self.hbox1,0,0)


        self.table = QtGui.QTableWidget()
        self.grid.addWidget(self.table, 1, 0)
        self.table.resize(1370, 190)
        self.table_item = QtGui.QTableWidgetItem()
        self.table.setRowCount(15)
        self.table.verticalHeader().hide()
        self.table.setColumnCount(6)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

        self.fnt = self.table.font()
        self.fnt.setPointSize(11)
        self.table.setFont(self.fnt)

        self.table.setHorizontalHeaderLabels(
            ("S.no, Item Description,Qty,Rate(Rs:),Subtotal,"",").split(','))
        all_data = [("1", "Acne-aid Wash Facial Cleansing", 191.72, 0),
                    ("2", "AMoisturizer", 90, 0),
                    ("3", "Brightening eye cream", 40, 0),
                    ("4", "Firming cream", 60, 0),
                    ("5", "Cleanser for oily skin", 50, 0),
                    ("6", "Serum", 23, 0),
                    ("7", "Cleansing wipes", 45, 0),
                    ("8", "Firming eye cream", 42, 0),
                    ("9", "Gentle cleanser", 12, 0),
                    ("10", "Dove cream", 69, 0),
                    ("11", "sandal cream", 45, 0),
                    ("12", "Lotus white cream", 89, 0),
                    ("13", "sandal powder", 62, 0),
                    ("14", "face wash", 35, 0),
                    ("15", "powder", 23, 0),
                    ("16", "fair and lovely cream", 42, 0)]
        for r, row_data in enumerate(all_data):
            for c, value in zip((0, 1, 3), row_data):
                it = QtGui.QTableWidgetItem(str(value))
                self.table.setItem(r, c, it)
        for r in range(self.table.rowCount()):
            spin = QtGui.QSpinBox(minimum=0, maximum=50)
            spin.valueChanged.connect(partial(self.calculateSubTotal, r))
            self.table.setCellWidget(r, 2, spin)

            btn = QtGui.QPushButton(icon=QtGui.QIcon("trash1.png"))
            self.table.setCellWidget(r, 5, btn)

        self.table.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.table.horizontalHeader().setStyleSheet(
            "QHeaderView { font-size:  18pt};")
        self.table.horizontalHeader().setStyleSheet(
            "::section {background-color : lightGray;font-size:13pt;}")
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.setStyleSheet("QTableWidget{ width:100%; height: 100px; }")
        self.table.horizontalHeader().setFixedHeight(40)

        self.table.setColumnWidth(0, 150)
        self.table.setColumnWidth(1, 330)
        self.table.setColumnWidth(2, 250)
        self.table.setColumnWidth(3, 210)
        self.table.setColumnWidth(4, 250)
        self.table.setColumnWidth(5, 20)
        self.grid.addWidget(self.table, 1, 0)


        # self.btn4.clicked.connect(self.on_pushButton_clicked)
        # self.dialog = Example1()

        self.hbox2 = QtGui.QHBoxLayout()
        self.btn2 = QtGui.QPushButton("back")
        self.hbox2.addWidget(self.btn2)
        self.btn2.setIcon(QtGui.QIcon("back.png"))
        self.btn2.setIconSize(QtCore.QSize(30, 40))

        self.btn2.setStyleSheet(ROUNDED_STYLE_SHEET6)
        self.btn3 = QtGui.QPushButton(" Reset Form")
        self.hbox2.addWidget(self.btn3)
        self.hbox2.addStretch()
        self.btn3.setStyleSheet(ROUNDED_STYLE_SHEET7)
        self.btn4 = QtGui.QPushButton("pay")
        self.btn4.clicked.connect((self.on_pushButton_clicked))
        self.btn4.setIcon(QtGui.QIcon("money.png"))
        self.btn4.setIconSize(QtCore.QSize(80, 50))
        self.hbox2.addWidget(self.btn4)
        self.grid.addLayout(self.hbox2, 2, 0)
        self.btn4.setStyleSheet(ROUNDED_STYLE_SHEET5)

        self.setWindowTitle("business management")
        self.setLayout(self.grid)
        self.dialog = Example1()
        self.dialog.setStyleSheet(QSS)
        # self.setGeometry(200, 300, 900, 600)
        # self.showMaximized()


    def calculateSubTotal(self, row, value):
        rate = float(self.table.item(row, 3).text())
        subtotal = value * rate

        item_subtotal = self.table.item(row, 4)
        if item_subtotal is None:
            item_subtotal = QtGui.QTableWidgetItem()
            self.table.setItem(row, 4, item_subtotal)

        item_subtotal.setText(str(subtotal))

    def on_pushButton_clicked(self):
        self.dialog.showMaximized()


def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    ex.show()
    ex.setGeometry(300,300,500,500)
    sys.exit(app.exec_())
    ex.setStyleSheet("background-color:white")


if __name__ == '__main__':
    main()




#
# self.grid2 = QtGui.QGridLayout()
# self.vbox1 = QtGui.QVBoxLayout()
# self.hbox3 = QtGui.QHBoxLayout()
# self.alignvbox = QtGui.QVBoxLayout()
# self.label = QtGui.QLabel("Payment Details")
# self.label.setStyleSheet("font: bold 30pt Comic Sans MS")
# self.hbox3.addWidget(self.label)
# self.hbox3.addStretch()
# self.hbox3.setSpacing(20)
# self.label2 = QtGui.QLabel("Amount : ")
# self.label2.setStyleSheet("font: bold 15pt Comic Sans MS")
# self.label3 = QtGui.QLabel("Quantity : ")
# self.label3.setStyleSheet("font: bold 15pt Comic Sans MS")
# self.amountvbox = QtGui.QVBoxLayout()
# self.amountvbox.setContentsMargins(5, 0, 0, 0)
# self.amountvbox.addStretch()
# self.amountvbox.addWidget(self.label2)
# self.quantityvbox = QtGui.QVBoxLayout()
# self.quantityvbox.setContentsMargins(5, 0, 0, 0)
# self.quantityvbox.addWidget(self.label3)
# # self.amountvbox.setAlignment(self.savebtn, QtCore.Qt.AlignCenter)
#
# self.vbox1.addLayout(self.amountvbox,QtCore.Qt.AlignTop)
# self.vbox1.addLayout(self.quantityvbox,QtCore.Qt.AlignTop)
# self.hbox3.addLayout(self.vbox1,QtCore.Qt.AlignRight)
#
# self.alignvbox.addLayout(self.hbox3,QtCore.Qt.AlignTop)
#
# self.alignvbox.addStretch()
# self.hbox4 = QtGui.QHBoxLayout()
# self.wbtn1 = QtGui.QPushButton("CASH")
# self.wbtn1.setStyleSheet(ROUNDED_STYLE_SHEET2)
# self.hbox4.addWidget(self.wbtn1)
# self.wbtn2 = QtGui.QPushButton("CARD")
# self.wbtn2.setStyleSheet(ROUNDED_STYLE_SHEET3)
# self.hbox4.addWidget(self.wbtn2)
# self.wbtn3 = QtGui.QPushButton("WALLET")
# self.wbtn3.setStyleSheet(ROUNDED_STYLE_SHEET4)
# self.hbox4.addWidget(self.wbtn3)
# self.alignvbox.addLayout(self.hbox4,QtCore.Qt.AlignTop)
# self.alignvbox.addStretch()
# self.vbox2 = QtGui.QVBoxLayout()
# self.label4 = QtGui.QLabel("Received Amount")
# self.vbox2.addWidget(self.label4)
# self.label4.setStyleSheet("font: bold 30pt Comic Sans MS")
# self.lb = QtGui.QLCDNumber()
# self.lb.setDigitCount(8)
# self.vbox2.addWidget(self.lb)
# self.vbox2.setAlignment(QtCore.Qt.AlignTop)
# self.hboxlayout = QtGui.QHBoxLayout()
# self.hboxlayout.addLayout(self.vbox2)
# self.hboxlayout.addStretch()
# self.layout = QtGui.QGridLayout()
# names = ['7', '8', '9',
#          '4', '5', '6',
#         '1', '2', '3',
#          '<--', '0', '.']
#
# positions = [(i,j) for i in range(4) for j in range(3)]
# for position, name in zip(positions, names):
#     button = QtGui.QPushButton(name)
#     self.layout.addWidget(button, *position)
# self.hboxlayout.addLayout(self.layout)
# self.alignvbox.addLayout(self.hboxlayout)
# self.saveVbox = QtGui.QVBoxLayout()
# self.savebtn = QtGui.QPushButton('Save')
#
# self.saveVbox.addWidget(self.savebtn)
# self.saveVbox.setAlignment(self.savebtn, QtCore.Qt.AlignCenter)
# self.alignvbox.addLayout(self.saveVbox)
# self.setLayout(self.alignvbox)

from pyface.qt import QtGui, QtCore
#
# app = QtGui.QApplication([])
#
# win = QtGui.QWidget()
# win.resize(300,300)
# layout = QtGui.QVBoxLayout(win)
# scroll = QtGui.QScrollArea()
# scroll.setWidgetResizable(True)
# layout.addWidget(scroll)
#
# scrollContents = QtGui.QWidget()
# layout = QtGui.QVBoxLayout(scrollContents)
# scroll.setWidget(scrollContents)
#
# pix = QtGui.QPixmap("./pos.png")
#
#
# w2 = QtGui.QWidget()
# label = QtGui.QLabel()
# label.setPixmap(pix)
# layout.addWidget(label)
# label2 = QtGui.QLabel("productname")
# layout.addWidget(label2)
# w2.addLayout(layout)
# scroll.setWidget(w2)
# i +=1
# break
#
#
# win.show()
# win.raise_()
# app.exec_()
# from PyQt4 import QtGui
# import sys
#
# imagePath = "./eggs.jpeg"
#
# class ImgWidget1(QtGui.QLabel):
#
#     def __init__(self, parent=None):
#         super(ImgWidget1, self).__init__(parent)
#         pic = QtGui.QPixmap(imagePath)
#         self.setPixmap(pic)
#
# class Widget(QtGui.QWidget):
#
#     def __init__(self):
#         super(Widget, self).__init__()
#         tableWidget = QtGui.QTableWidget(3, 2, self)
#         tableWidget.setCellWidget(0, 0, ImgWidget1(self))
#         tableWidget.setCellWidget(0, 1, ImgWidget1(self))
#         tableWidget.setCellWidget(1, 0, ImgWidget1(self))
#         tableWidget.setCellWidget(1, 1, ImgWidget1(self))
#
#
# if __name__ == "__main__":
#     app = QtGui.QApplication([])
#     wnd = Widget()
#     wnd.show()
#     sys.exit(app.exec_())
import sys


class QCustomQWidget (QtGui.QWidget):
    def __init__ (self, parent = None):
        super(QCustomQWidget, self).__init__(parent)
        self.textQVBoxLayout = QtGui.QVBoxLayout()
        self.textUpQLabel    = QtGui.QLabel()
        # self.textDownQLabel  = QtGui.QLabel()
        self.textQVBoxLayout.addWidget(self.textUpQLabel)
        # self.textQVBoxLayout.addWidget(self.textDownQLabel)

        self.allQHBoxLayout  = QtGui.QHBoxLayout()
        self.iconQLabel      = QtGui.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.setLayout(self.allQHBoxLayout)
        # setStyleSheet
        self.textUpQLabel.setStyleSheet('''
            color: rgb(0, 0, 255);
        ''')

    def setTextUp (self, text):
        self.textUpQLabel.setText(text)

    # def setTextDown (self, text):
    #     self.textDownQLabel.setText(text)

    def setIcon (self, imagePath):
        self.iconQLabel.setPixmap(QtGui.QPixmap(imagePath))

class exampleQMainWindow (QtGui.QMainWindow):
    def __init__ (self):
        super(exampleQMainWindow, self).__init__()
        # Create QListWidget
        self.myQListWidget = QtGui.QListWidget(self)
        for index, icon in [
            ('No.1',   'eggs.jpeg'),
            ('No.2',  'food.jpg'),
            ('No.3', 'ghee.jpg')]:
            # Create QCustomQWidget
            myQCustomQWidget = QCustomQWidget()
            myQCustomQWidget.setTextUp(index)
            myQCustomQWidget.setIcon(icon)
            # Create QListWidgetItem
            myQListWidgetItem = QtGui.QListWidgetItem(self.myQListWidget)
            # Set size hint
            myQListWidgetItem.setSizeHint(myQCustomQWidget.sizeHint())
            # Add QListWidgetItem into QListWidget
            # self.myQListWidget.addItem(myQListWidgetItem)
            self.myQListWidget.setItemWidget(myQListWidgetItem, myQCustomQWidget)
        self.setCentralWidget(self.myQListWidget)

app = QtGui.QApplication([])
window = exampleQMainWindow()
window.show()
sys.exit(app.exec_())

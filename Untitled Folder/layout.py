import sys
from functools import partial
from PyQt4 import QtGui, QtCore

# from simple_table import Settings

ROUNDED_STYLE_SHEET1 = """QPushButton {
     background-color: yellow;
     color: brown;
     border-style: outset;
     border-width: 4px
     border-radius: 15px;
     border-color: blue;
     font: bold 17px;
     min-width: 7em;
     padding: 8px;
     border:none;outline : None;
 }
"""
ROUNDED_STYLE_SHEET2 = """QPushButton {
     background-color: green;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 12px;
     min-width: 25em;
     padding: 25px;
     border:none;outline : None;
 }
"""

ROUNDED_STYLE_SHEET3 = """QPushButton {
     background-color: orange;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 12px;
     min-width: 25em;
     padding: 25px;
     border:none;outline : None;
 }
"""
ROUNDED_STYLE_SHEET4 = """QPushButton {
     background-color: blue;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 12px;
     min-width: 25em;
     padding: 25px;
     border:none;outline : None;
 }
"""
ROUNDED_STYLE_SHEET5 = """QPushButton {
     background-color: blue;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 20px;
     min-width: 8em;
     padding: 8px;
     border:none;outline : None;
 }
"""
ROUNDED_STYLE_SHEET6 = """QPushButton {
     background-color: red;
     color: white;
     border-style: outset;
     border-width: 4px;
     border:none;outline : None;
     border-radius: 15px;
     border-color: blue;
     font: bold 17px;
     min-width: 8em;
     padding: 10px;
 }
"""
ROUNDED_STYLE_SHEET7 = """QPushButton {
     background-color: brown;
     color: white;
     border-style: outset;
     border-width: 4px;
     border:none;outline : None;
     border-radius: 15px;
     border-color: red;
     font: bold 17px;
     min-width: 10em;
     padding: 18px;
 }
"""
OVAL = """QPushButton {

      position: relative;
      width: 100px;
      height: 70px;
      margin: 20px 0;
      border:none;outline : None;
      background: green;
      border-radius: 48% / 25%;
      color: white;
      border-color: blue;
      font: bold 20px;
}
"""


#
# class Example1(QtGui.QWidget):
#
#     def __init__(self):
#         super(Example1, self).__init__()
#         self.initUI()
#
#     def initUI(self):
#         self.grid2 = QtGui.QGridLayout()
#         self.vbox1 = QtGui.QVBoxLayout()
#         self.hbox3 = QtGui.QHBoxLayout()
#         self.alignvbox = QtGui.QVBoxLayout()
#         self.hbox3.setContentsMargins(0,0,0,0)
#         self.label = QtGui.QLabel("Payment Details")
#         self.label.setStyleSheet("font: bold 30pt Comic Sans MS")
#         self.hbox3.addWidget(self.label,0,QtCore.Qt.AlignTop)
#
#         self.box = QtGui.QVBoxLayout()
#
#         self.label2 = QtGui.QLabel(alignment=QtCore.Qt.AlignRight)
#         self.label2.setText("Amount :")
#
#         self.label2.setStyleSheet("font: bold 15pt Comic Sans MS")
#         self.box.addWidget(self.label2)
#         self.grid2.addLayout(self.hbox3,0,0)
#
#         self.label3 = QtGui.QLabel(alignment=QtCore.Qt.AlignRight)
#         self.label3.setText("Quantity :")
#         self.label3.setStyleSheet("font: bold 15pt Comic Sans MS")
#         self.box.addWidget(self.label3)
#         self.hbox3.addLayout(self.box,QtCore.Qt.AlignTop)
#
#         self.hbox4 = QtGui.QHBoxLayout()
#         self.wbtn1 = QtGui.QPushButton("CASH")
#         self.hbox4.addWidget(self.wbtn1,QtCore.Qt.AlignTop)
#         self.wbtn2 = QtGui.QPushButton("CARD")
#         self.hbox4.addWidget(self.wbtn2,QtCore.Qt.AlignTop)
#         self.wbtn3 = QtGui.QPushButton("WALLET")
#
#         self.hbox4.addWidget(self.wbtn3,QtCore.Qt.AlignTop)
#         self.grid2.addLayout(self.hbox4,1,0)
#
#         self.setLayout(self.grid2)
# def main():
#   app = QtGui.QApplication(sys.argv)
#   ex = Example1()
#   ex.show()
#   ex.setGeometry(300,300,500,500)
#   sys.exit(app.exec_())
#   ex.setStyleSheet("background-color:white")
#
# if __name__ == '__main__':
#   main()
import sys
from PyQt4 import QtCore, QtGui

QSS = '''
QLabel#big{
    font: bold 20pt Comic Sans MS
}
QLabel#small{
    font: bold 15pt Comic Sans MS
}
'''

class Example1(QtGui.QWidget):
    def __init__(self, parent=None):
        super(Example1, self).__init__(parent)
        self.initUI()

    def initUI(self):
        grid = QtGui.QGridLayout(self)
        label_payment = QtGui.QLabel("Payment Details", objectName="big")
        label_received = QtGui.QLabel("Received Amount", objectName="big")

        label_amount = QtGui.QLabel("Amount: 192", objectName="small")
        label_quantity = QtGui.QLabel("Quantity: 1", objectName="small")

        cash_button = QtGui.QPushButton("Cash")
        card_button = QtGui.QPushButton("Card")
        wallet_button = QtGui.QPushButton("Wallet")

        lcd = QtGui.QLCDNumber()
        sp = lcd.sizePolicy()
        sp.setHorizontalPolicy(QtGui.QSizePolicy.Fixed)
        lcd.setSizePolicy(sp)

        grid_buttons = QtGui.QGridLayout()

        names = ['7', '8', '9',
                 '4', '5', '6',
                '1', '2', '3',
                 '<--', '0', '.']
        positions = [(i,j) for i in range(4) for j in range(3)]
        for position, name in zip(positions, names):
            button = QtGui.QPushButton(name)
            grid_buttons.addWidget(button, *position)

        grid.addWidget(label_payment, 0, 0, 2, 3)
        grid.addWidget(label_amount, 0, 5)
        grid.addWidget(label_quantity, 1, 5)

        hbox = QtGui.QHBoxLayout(spacing=0)
        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.addWidget(cash_button)
        hbox.addWidget(card_button)
        hbox.addWidget(wallet_button)
        grid.addLayout(hbox, 3, 0, 1, 6)

        vbox = QtGui.QVBoxLayout(spacing=0)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(label_received)
        vbox.addWidget(lcd)
        grid.addLayout(vbox, 4, 1)

        grid.addLayout(grid_buttons, 4, 3, 4, 3)
        self.setFixedSize(self.sizeHint())

def main():
    app = QtGui.QApplication(sys.argv)
    app.setStyleSheet(QSS)
    ex = Example1()
    ex.show()
    ex.setGeometry(300,300,500,500)
    sys.exit(app.exec_())

if __name__ == '__main__': main()

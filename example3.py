from datetime import datetime
from PyQt4 import QtCore, QtGui
from functools import partial
import glob, os
global typ,showdetails
typ = "product"
showdetails = False
class ClickableLabel(QtGui.QLabel):
    clicked = QtCore.pyqtSignal()

    def mousePressEvent(self, event):
        self.clicked.emit()
        super(ClickableLabel, self).mousePressEvent(event)

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        # self.data_arraylist = {'8074435207':{'item1':"Fruites", 'Qty':'10','Price':'100','total':'100'},'9703762195':{'item1':"vegitables", 'Qty':'2kg','Price':'200','total':'200'}}
        self.data_arraylist = {'8074435207':{'item1':"Fruites", 'Qty':'10','Price':'100','total':'100'},'9703762195':{'item1':"vegitables", 'Qty':'2kg','Price':'200','total':'200'},'9705290879':{'item1':"snacks", 'Qty':'10p','Price':'200','total':'200'}}
        super(MainWindow, self).__init__(parent)
        self.scrollArea_left = QtGui.QScrollArea(widgetResizable=True)
        self.content_widget_left = QtGui.QWidget()
        self.scrollArea_left.setWidget(self.content_widget_left)
        self.vbox = QtGui.QVBoxLayout(self.content_widget_left)
        self._layout = QtGui.QGridLayout()
        self.linedit = QtGui.QLineEdit()
        self._label_pixmap = QtGui.QLabel(alignment=QtCore.Qt.AlignCenter)
        self._label_text = QtGui.QLabel(alignment=QtCore.Qt.AlignCenter)
        self._label_itemDes = QtGui.QLabel(alignment=QtCore.Qt.AlignCenter)
        widget = QtGui.QWidget()
        self.vbox1 = QtGui.QVBoxLayout(widget)
        self.vbox1.addWidget(self._label_pixmap)
        self.vbox1.addWidget(self._label_text)
        self.vbox1.addWidget(self._label_itemDes)
        self.searchbox = QtGui.QHBoxLayout()
        self.searchlabel = QtGui.QLabel("Search")
        self.searchedit = QtGui.QLineEdit()
        self.searchbox.addWidget(self.searchlabel)
        self.searchbox.addWidget(self.searchedit)
        # self.vbox.addLayout(self.searchbox)
        self.vbox.addLayout(self._layout)

        self.timelabel = QtGui.QLabel()
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.displayTime)
        self.timer.start()

        self.hboxbtns = QtGui.QHBoxLayout()

        self.neworderbtn = QtGui.QPushButton("Orders")
        self.neworderbtn.clicked.connect(self.orders_data)
        self.finalorderbtn = QtGui.QPushButton("New")
        self.finalorderbtn.clicked.connect(self.new_data)
        self.hboxbtns.addWidget(self.timelabel)

        self.hboxbtns.addWidget(self.neworderbtn)
        self.hboxbtns.addWidget(self.finalorderbtn)

        w = QtGui.QWidget()
        vbox = QtGui.QVBoxLayout(w)
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.scrollArea_left)
        hbox.addWidget(widget)
        vbox.addLayout(hbox)
        vbox.addLayout(self.hboxbtns)

        self.setCentralWidget(w)

        highlight_dir = '/home/cioc/Documents/pos/images'
        self._it = QtCore.QDirIterator(highlight_dir)

        self._row, self._col = 0, 0
        QtCore.QTimer(self, interval=10, timeout=self.load_image).start()
        completer = QtGui.QCompleter()
        self.searchedit.setCompleter(completer)

        model = QtGui.QStringListModel()
        completer.setModel(model)
        self.get_data(model)
    def displayTime(self):

        cur_time = datetime.strftime(datetime.now(), "%I:%M:%S %p ")
        self.timelabel.setText(cur_time)

    def keyPressEvent(self, event):
        print event,event.key(),QtCore.Qt.Key_Return
        if event.key() == QtCore.Qt.Key_Return:
            text_key = self.linedit.text()
            self.searchitems(text_key)
            print text_key, "ph numbers  textttttttttttttttttt"
    def keyPressEvent1(self, event1):
        print event,event.key(),QtCore.Qt.Key_Return
        if event.key() == QtCore.Qt.Key_Return:
            txt = self.searchedit.text()

            print txt, "imagesssssssssssss"

    def get_data(self,model):
        for dirpath,_,filenames in os.walk('/home/cioc/Documents/pos/images'):
           model.setStringList(filenames)

    def searchFile(self,txt):

        self.browsertext = str(txt)

        for dirpath,_,filenames in os.walk('/home/cioc/Documents/pos/images'):
            for file in filenames:
                s=str(os.path.abspath(os.path.join(dirpath, file)))
                if self.browsertext in s:
                    self._label_text.setText(file)
                    pix = QtGui.QPixmap(str(s))
                    self._label_pixmap.setPixmap(pix)

    def load_image(self):
        if self._it.hasNext():
            self.pixmap = QtGui.QPixmap(self._it.next())
            if not self.pixmap.isNull():
                vlay = QtGui.QVBoxLayout()
                self.label_pixmap = ClickableLabel(alignment=QtCore.Qt.AlignCenter, pixmap=self.pixmap)
                self.label_text = QtGui.QLabel(alignment=QtCore.Qt.AlignCenter, text=self._it.fileName())
                self.label_pixmap.clicked.connect(partial(self.on_clicked, self._it.fileName(), self.pixmap))
                vlay.addWidget(self.label_pixmap)
                vlay.addWidget(self.label_text)
                vlay.addStretch()
                self._layout.addLayout(vlay, self._row, self._col)
                self._col += 1
                if self._col == 3:
                    self._row += 1
                    self._col = 0
    def on_clicked(self, txt,pixmap):
        self._label_pixmap.setPixmap(pixmap)
        self._label_text.setText(str("item: ")+txt)
        des = "Store in a cool, dry places\n Very healthy food\n quantity:1kg"
        self._label_itemDes.setText(str(des))

    def orders_data(self):
            self.mainw = QtGui.QWidget()
            self.scrollArea_left.setWidget(self.mainw)
            self.ordersvbox = QtGui.QGridLayout(self.mainw)
            self.w1 = QtGui.QWidget()
            self.w1.mousePressEvent=self.myfunction1
            self.v1 = QtGui.QVBoxLayout(spacing=0)
            self.v1.setContentsMargins(0, 0, 0, 0)
            self.w1.setLayout(self.v1)
            self.h1 = QtGui.QHBoxLayout()
            self.l1 = QtGui.QLabel("#23")
            self.lprogress = QtGui.QLabel("Progress")
            self.lamount = QtGui.QLabel("340 RS")
            self.items_count = QtGui.QLabel(" 1 items")
            self.h1.addWidget(self.l1)
            self.h1.addWidget(self.lprogress)
            self.h1.addWidget(self.lamount)
            self.v1.addLayout(self.h1)
            self.v1.addWidget(self.items_count)
            line = QtGui.QFrame()
            line.setFrameShape(QtGui.QFrame.HLine)
            self.ordersvbox.addWidget(self.w1,0,0)
            self.ordersvbox.addWidget(line,1,0)

            self.w2 = QtGui.QWidget()
            # self.w1.mousePressEvent=self.myfunction2
            self.v2 = QtGui.QVBoxLayout(self.w2)
            self.h2 = QtGui.QHBoxLayout()
            self.l2 = QtGui.QLabel("#24")
            self.lprogress2 = QtGui.QLabel("Deliver")
            self.lamount2 = QtGui.QLabel("300 RS")
            self.items_count2 = QtGui.QLabel(" 1 items")
            self.h2.addWidget(self.l2)
            self.h2.addWidget(self.lprogress2)
            self.h2.addWidget(self.lamount2)
            self.v2.addLayout(self.h2)
            self.v2.addWidget(self.items_count2)
            self.ordersvbox.addWidget(self.w2,2,0)

            # self.w3 = QtGui.QWidget()
            # self.v3 = QtGui.QVBoxLayout(self.w3)
            # self.h3 = QtGui.QHBoxLayout()
            # self.l3 = QtGui.QLabel("#25")
            # self.lprogress3 = QtGui.QLabel("Out Forward")
            # self.lamount3 = QtGui.QLabel("200 RS")
            # self.items_count3 = QtGui.QLabel(" 2 items")
            # self.h3.addWidget(self.l3)
            # self.h3.addWidget(self.lprogress3)
            # self.h3.addWidget(self.lamount3)
            # self.v3.addLayout(self.h3)
            # self.v3.addWidget(self.items_count3)
            # self.ordersvbox.addWidget(self.w3,2,0)

    def myfunction1(self,event):
        print " hello ",self.l1
        txt = str(self.l1.text())
        self.dialog1 = QtGui.QDialog()
        self.layout = QtGui.QVBoxLayout(self.dialog1)
        self.orderphlabel = QtGui.QLabel(txt +"      phno: 8074435207")
        self.orderpersonname = QtGui.QLabel("Name : Gowthami")

        self.layout.addWidget(self.orderphlabel)
        self.layout.addWidget(self.orderpersonname)

        self.desitem1 = QtGui.QLabel("item1 : Fruites")
        self.desitem2 = QtGui.QLabel("item2 : vegitables")

        self.layout.addWidget(self.desitem1)
        # self.layout.addWidget(self.desitem2 )
        self.printbtn = QtGui.QPushButton("Print")
        self.layout.addWidget(self.printbtn)

        self.dialog1.setGeometry(500,600,300,300)
        self.dialog1.exec_()


    def new_data(self):
        self.mainw2 = QtGui.QWidget()
        self.scrollArea_left.setWidget(self.mainw2)
        self.newvbox = QtGui.QVBoxLayout(self.mainw2)
        self.linedit = QtGui.QLineEdit()
        self.search = QtGui.QLabel("Search")
        self.newhbox = QtGui.QHBoxLayout()
        self.newhbox.addWidget(self.linedit)
        self.newhbox.addWidget(self.search)
        self.newvbox.addLayout(self.newhbox)


        self.table = QtGui.QTableWidget()
        self.table_item = QtGui.QTableWidgetItem()
        self.table.setRowCount(5)
        self.table.verticalHeader().hide()
        self.table.setColumnCount(5)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.table.setColumnWidth(0, 100)
        self.table.setColumnWidth(1, 200)
        self.table.setColumnWidth(2, 120)
        self.table.setColumnWidth(3, 150)
        self.table.setColumnWidth(4, 150)
        self.table.setColumnWidth(5, 100)

        self.fnt = self.table.font()
        self.fnt.setPointSize(15)
        self.table.setFont(self.fnt)

        self.table.setHorizontalHeaderLabels(("S.no, Item Description,Qty,Rate(Rs:),Total").split(','))
        self.newvbox.addWidget(self.table)
        self.newsavebtn = QtGui.QPushButton("Save")
        self.newsavebtn.clicked.connect(self.cards_selection)


        self.newvbox.addWidget(self.newsavebtn)
    def searchitems(self,text_key):
        print type(text_key), "search"
        print text_key, "keyyyy"
        print  self.data_arraylist, 'dictionaryyyyyyyyyyyyyyyyyyyy'
        if str(text_key) in  self.data_arraylist:
            print True
            for ridx,row in enumerate(range(1)):
                for idx, column in enumerate(range(5)):
                    if idx ==0:

                        self.table.setItem(row,column,QtGui.QTableWidgetItem(str(ridx+1)))
                    elif idx ==1:
                        self.table.setItem(row,column,QtGui.QTableWidgetItem(str(self.data_arraylist[str(text_key)]['item1'])))

                    elif idx ==2:
                        self.table.setItem(row,column,QtGui.QTableWidgetItem(str(self.data_arraylist[str(text_key)]['Qty'])))
                    elif idx ==3:
                        self.table.setItem(row,column,QtGui.QTableWidgetItem(str(self.data_arraylist[str(text_key)]['Price'])))
                    elif idx ==4:
                        self.table.setItem(row,column,QtGui.QTableWidgetItem(str(self.data_arraylist[str(text_key)]['total'])))


        else:
            self.dialogerror = QtGui.QDialog()
            self.errorvbox = QtGui.QVBoxLayout(self.dialogerror)
            self.labeleroor = QtGui.QLabel("Please Enter Valid PhoneNumber")
            self.errorvbox.addWidget(self.labeleroor)
            self.dialogerror.setGeometry(500,650,300,100)
            self.dialogerror.exec_()
            print False


    def cards_selection(self):
        self.cardswidget = QtGui.QWidget()
        self.cardshbox = QtGui.QHBoxLayout(self.cardswidget)
        self.pushbutnwallet = QtGui.QPushButton("WALLET")
        self.cashbtn = QtGui.QPushButton("CASH")
        self.cardbtn = QtGui.QPushButton("CARD")
        self.cardshbox.addWidget(self.pushbutnwallet)
        self.cardshbox.addWidget(self.cashbtn)
        self.cardshbox.addWidget(self.cardbtn)
        self.cardswidget.setGeometry(500,500,600,100)
        self.cardswidget.show()


if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)

    w = MainWindow()

    w.show()
    w.setGeometry(500,500,800,400)
    sys.exit(app.exec_())

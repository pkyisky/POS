# from datetime import datetime
# from PyQt4 import QtCore, QtGui
# from functools import partial
# import glob, os
# global typ,showdetails
# typ = "product"
# from functools import partial
# class Table_Program(QtGui.QMainWindow):
#     def __init__(self, parent=None):
#         super(Table_Program, self).__init__(parent)
#         self.scrollArea_left = QtGui.QScrollArea(widgetResizable=True)
#
#         self.content_widget_left = QtGui.QWidget()
#
#         self.scrollArea_left.setWidget(self.content_widget_left)
#         self.vbox = QtGui.QVBoxLayout(self.content_widget_left)
#         self.showFullScreen()
#         self.timelabel = QtGui.QLabel()
#         self.timer = QtCore.QTimer(self)
#         self.font = QtGui.QFont()
#         self.font.setPointSize(18)
#         self.font.setBold(True)
#         self.timelabel.setFont(self.font)
#
#         self.timer.setInterval(1000)
#         self.timer.timeout.connect(self.displayTime)
#         self.timer.start()
#         self.hboxbtns = QtGui.QHBoxLayout()
from datetime import datetime
from PyQt4 import QtCore, QtGui
from functools import partial
import glob, os
global typ,showdetails
typ = "product"
showdetails = False
# class ClickableLabel(QtGui.QLabel):
#     clicked = QtCore.pyqtSignal()
#
#     def mousePressEvent(self, event):
#         self.clicked.emit()
#         super(ClickableLabel, self).mousePressEvent(event)

class Table_Program(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Table_Program, self).__init__(parent)

        global settingobj,data_arraylist
        settingobj.close()
        data_arraylist = {}
        try:
            with open("dictionary.txt","r") as f:
                js = json.load(f)
                data_arraylist = js
                print data_arraylist,type(data_arraylist)
        except:
            with open("dictionary.txt","w") as f:
                json.dump(data_arraylist,f)
                print 'file createddddddddd'
        self.scrollArea_left = QtGui.QScrollArea(widgetResizable=True)

        self.mainw2 = QtGui.QWidget()
        self.mainw2.showFullScreen()
        self.totalvbox = QtGui.QVBoxLayout(self.mainw2)
        self.w = QtGui.QWidget()
        self.scrollArea_left.setWidget(self.w)

        self.mainw2.setStyleSheet("QWidget {background-color: black;color: white;}")

        self.newvbox = QtGui.QVBoxLayout(self.w)
        self.table = QtGui.QTableWidget()
        self.table.setStyleSheet("QTableWidget{gridline-color: white}")
        self.table_item = QtGui.QTableWidgetItem()
        self.table.setRowCount(2)
        self.table.verticalHeader().hide()
        self.table.setColumnCount(6)
        self.table.setHorizontalHeaderLabels(("S.no, Item Description,Qty,Rate(Rs:),Subtotal,"",").split(','))
        self.serachhbox = QtGui.QHBoxLayout()
        self.labels = QtGui.QHBoxLayout()
        self.cioclabel = QtGui.QLabel("CIOC")

        self.poslabel = QtGui.QLabel("POS")
        self.poslabel.setAlignment(QtCore.Qt.AlignCenter)

        self.invoicelabel = QtGui.QLabel('Invoice Serial Number - 39')
        self.invoicelabel.setAlignment(QtCore.Qt.AlignRight)

        self.labels.addWidget(self.cioclabel)
        self.labels.addWidget(self.poslabel)
        self.labels.addWidget(self.invoicelabel)

        self.phnbr = QtGui.QLabel("Phone No:")
        self.searchlabel = QtGui.QLabel("Search")
        self.searchEdit = QtGui.QLineEdit()
        self.textEdit = QtGui.QTextEdit()

        self.textEdit.setFixedSize(1860,150)

        self.serachhbox.addWidget(self.phnbr)
        self.serachhbox.addWidget(self.searchEdit)
        self.serachhbox.addWidget(self.searchlabel)
        self.newvbox.addLayout(self.labels)
        self.newvbox.addLayout(self.serachhbox)
        self.newvbox.addWidget(self.textEdit)
        self.newvbox.addWidget(self.table)
        header = self.table.horizontalHeader()
        header.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Plain)
        header.setLineWidth(1)
        self.table.setHorizontalHeader(header)
        stylesheet = "::section{Background-color:black;color: white}"
        self.table.horizontalHeader().setStyleSheet(stylesheet);
        header.setResizeMode(1, QtGui.QHeaderView.Stretch)
        header.setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(2, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(3, QtGui.QHeaderView.Stretch)
        header.setResizeMode(4, QtGui.QHeaderView.Stretch)
        vh = self.table.verticalHeader()
        vh.setDefaultSectionSize(50)
        self.btn1 = QtGui.QPushButton(icon=QtGui.QIcon("./plus1.png"))
        self.btn1.setIconSize(QtCore.QSize(60,30))
        self.btn1.setStyleSheet("QPushButton {outline : None;}")
        self.table.setCellWidget(1,0,self.btn1)
        self.table.setItem(0,0,QtGui.QTableWidgetItem(str(1)))
        self.btn = QtGui.QPushButton(icon=QtGui.QIcon("./delete.png"))
        self.btn.setIconSize(QtCore.QSize(40,60))
        self.table.setCellWidget(0,5,self.btn)
        self.quantybutton = QtGui.QPushButton("3")
        index = QtCore.QPersistentModelIndex(self.table.model().index(0, 5))
        self.table.setCellWidget(0, 2,self.quantybutton)
        self.hbox2 = QtGui.QHBoxLayout()
        self.btn2 = QtGui.QPushButton("Inventory")
        self.hbox2.addWidget(self.btn2)
        self.btn3 = QtGui.QPushButton(" Reset Form")
        self.hbox2.addWidget(self.btn3)
        self.hbox2.addStretch()
        self.paybtn4 = QtGui.QPushButton("Pay")
        # self.paybtn4.clicked.connect(self.cards_selection)
        self.paybtn4.setIcon(QtGui.QIcon("money.png"))
        self.paybtn4.setIconSize(QtCore.QSize(20, 30))
        self.hbox2.addWidget(self.paybtn4)
        self.newvbox.addLayout(self.hbox2)

        self.hbox = QtGui.QHBoxLayout()


        self.timelabel = QtGui.QLabel()
        self.timer = QtCore.QTimer(self)
        self.font = QtGui.QFont()
        self.font.setPointSize(18)
        self.font.setBold(True)
        self.timelabel.setFont(self.font)

        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.displayTime)
        self.timer.start()
        self.orderbtn = QtGui.QPushButton("Orders")
        self.newbtn = QtGui.QPushButton("New")
        # self.newbtn.clicked.connect(self.new_data)
        self.hbox.addWidget(self.timelabel)
        self.hbox.addWidget(self.orderbtn)
        self.hbox.addWidget(self.newbtn)
        self.w.showFullScreen()

        self.totalvbox.addWidget(self.scrollArea_left)
        self.totalvbox.addLayout(self.hbox)

        self.setCentralWidget(self.mainw2)
    def keyPressEvent(self, event):
        # print event,event.key(),QtCore.Qt.Key_Return
        if event.key() == QtCore.Qt.Key_Return:
            text_key =self.searchEdit.text()
            self.searchitems(text_key)
    def displayTime(self):
        cur_time = datetime.strftime(datetime.now(), "%I:%M:%S %p ")
        self.timelabel.setText(cur_time)

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)

    w = Table_Program()

    w.show()
    w.showFullScreen()
    sys.exit(app.exec_())

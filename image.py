import os
import sys
from PyQt4 import QtGui, QtCore


class BasePanel(QtGui.QWidget):
    """This is more or less abstract, subclass it for 'panels' in the main UI"""
    def __init__(self, parent=None):
        super(BasePanel, self).__init__(parent)
        self.frame_layout = QtGui.QVBoxLayout()
        self.frame_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.frame_layout)

        self.frame = QtGui.QFrame()
        self.frame.setObjectName("base_frame")
        self.frame.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Plain)
        self.frame.setLineWidth(1)
        self.frame_layout.addWidget(self.frame)

        self.base_layout = QtGui.QVBoxLayout()
        self.frame.setLayout(self.base_layout)


class RightPanel(BasePanel):
    def __init__(self, parent=None):
        super(RightPanel, self).__init__(parent)

        self.qvw1 = QtGui.QWidget()
        self.scrollArea = QtGui.QScrollArea(widgetResizable=True)
        self.scrollArea.setWidget(self.qvw1)

        self.online_order_hbox = QtGui.QVBoxLayout(self.qvw1)
        self.online_label = QtGui.QPushButton("Online Order")
        self.online_label.setStyleSheet("QPushButton{ background-color: #FF8C00; color: white;outline : None;}")
        self.online_order_hbox.addWidget(self.online_label,QtCore.Qt.AlignTop)
        self.qvw1.setFixedWidth(round((300)))
        self.w1 = QtGui.QWidget()
        self.w1_vbox=QtGui.QVBoxLayout(self.w1)
        self.date_label1 = QtGui.QLabel("Jan 24,2019           #175", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping = QtGui.QLabel("Shopping :online")
        self.ordercity = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label1)
        self.w1_vbox.addWidget(self.amount)
        self.w1_vbox.addWidget(self.shopping)
        self.w1_vbox.addWidget(self.ordercity)
        self.w1_vbox.addWidget(self.orderstate)
        self.online_order_hbox.addWidget(self.w1)

        self.base_layout.addWidget(self.w1)


        self.w2 = QtGui.QWidget()
        self.w2_vbox=QtGui.QVBoxLayout(self.w2)
        self.date_label2 = QtGui.QLabel("Jan 30,2019           #178", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount2 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping2 = QtGui.QLabel("Shopping :online")
        self.ordercity2 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate2 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label2)
        self.w1_vbox.addWidget(self.amount2)
        self.w1_vbox.addWidget(self.shopping2)
        self.w1_vbox.addWidget(self.ordercity2)
        self.w1_vbox.addWidget(self.orderstate2)
        self.w1.resize(100,50)
        self.online_order_hbox.addWidget(self.w2)
        self.base_layout.addWidget(self.w2)

class MainApp(QtGui.QMainWindow):
    def __init__(self):
        super(MainApp, self).__init__()

        main_layout = QtGui.QHBoxLayout()
        central_widget = QtGui.QWidget()
        central_widget.setLayout(main_layout)
        self.setCentralWidget(central_widget)


        right_panel = RightPanel()
        main_layout.addWidget(right_panel)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    ex = MainApp()
    ex.show()

    sys.exit(app.exec_())

from datetime import datetime
from PyQt4 import QtCore, QtGui
from functools import partial
import glob, os
from functools import partial
class MainWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        tv = QtGui.QTableView()
        header = ["S.no", "Item Description","Qty","Rate(Rs:)","Subtotal"]
        tm = MyTableModel(self.tabledata, header, self)
        tv.setModel(tm)
        font = QFont("Courier New", 8)
        tv.setFont(font)
        vh = tv.verticalHeader()
        vh.setVisible(False)
        hh = tv.horizontalHeader()
        hh.setStretchLastSection(True)
        tv.resizeColumnsToContents()
        

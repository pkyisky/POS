from datetime import datetime
from PyQt4 import QtCore, QtGui
from functools import partial
import glob, os
from functools import partial
class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.scrollArea_left = QtGui.QScrollArea(widgetResizable=True)
        self.content_widget_left = QtGui.QWidget()
        self.scrollArea_left.setWidget(self.content_widget_left)
        self.vbox = QtGui.QVBoxLayout(self.content_widget_left)
        self._layout = QtGui.QGridLayout()
        widget = QtGui.QWidget()
        self.hboxbtns = QtGui.QHBoxLayout()
        self.neworderbtn = QtGui.QPushButton("Orders")
        self.finalorderbtn = QtGui.QPushButton("New")
        self.finalorderbtn.clicked.connect(self.new_data)
        self.hboxbtns.addWidget(self.neworderbtn)
        self.hboxbtns.addWidget(self.finalorderbtn)
        w = QtGui.QWidget()
        vbox = QtGui.QVBoxLayout(w)
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.scrollArea_left)
        hbox.addWidget(widget)
        vbox.addLayout(hbox)
        vbox.addLayout(self.hboxbtns)
        self.setCentralWidget(w)
    def new_data(self):
        self.mainw2 = QtGui.QWidget()
        self.scrollArea_left.setWidget(self.mainw2)
        self.newvbox = QtGui.QVBoxLayout(self.mainw2)
        self.table = QtGui.QTableWidget()
        self.table_item = QtGui.QTableWidgetItem()
        self.table.setRowCount(2)
        self.table.verticalHeader().hide()
        self.table.setColumnCount(6)
        self.fnt = self.table.font()
        self.fnt.setPointSize(20)
        self.table.setFont(self.fnt)
        self.table.setHorizontalHeaderLabels(("S.no, Item Description,Qty,Rate(Rs:),Subtotal,"",").split(','))
        self.newvbox.addWidget(self.table)
        header = self.table.horizontalHeader()
        header.setResizeMode(1, QtGui.QHeaderView.Stretch)
        header.setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(2, QtGui.QHeaderView.Stretch)
        header.setResizeMode(3, QtGui.QHeaderView.Stretch)
        header.setResizeMode(4, QtGui.QHeaderView.Stretch)
        self.btn1 = QtGui.QPushButton(icon=QtGui.QIcon("./plus.png"))
        self.table.setCellWidget(1,0,self.btn1)
        self.table.setItem(0,0,QtGui.QTableWidgetItem(str(1)))
        self.btn1.clicked.connect(self.insert_rows)
        self.btn = QtGui.QPushButton(icon=QtGui.QIcon("./delete.png"))
        self.table.setCellWidget(0,5,self.btn)

        index = QtCore.QPersistentModelIndex(self.table.model().index(0, 5))
        self.btn.clicked.connect(lambda: self.handleButton(index))

        self.spin = QtGui.QSpinBox(minimum=0, maximum=50)
        ct = self.table.rowCount()
        self.spin.valueChanged.connect(partial(self.calculateSubTotal, ct))
        self.table.setCellWidget(0, 2, self.spin)

    def handleButton(self, index):
        print('button clicked:', index.row())
        if index.isValid():
            self.table.removeRow(index.row())

    def insert_rows(self):
        ct = self.table.rowCount()
        print ct
        self.table.insertRow(self.table.rowCount()-1)
        self.btn = QtGui.QPushButton(icon=QtGui.QIcon("./delete.png"))
        self.table.setItem(ct-1,0,QtGui.QTableWidgetItem(str(ct)))
        self.table.setCellWidget(ct -1,5,self.btn)
        index = QtCore.QPersistentModelIndex(self.table.model().index(ct -1, 5))
        self.btn.clicked.connect(lambda: self.handleButton1(index))
        self.spin = QtGui.QSpinBox(minimum=0, maximum=50)
        self.table.setCellWidget(ct -1, 2, self.spin)

    def handleButton1(self, index):
        print('button clicked:', index.row())
        if index.isValid():
            self.table.removeRow(index.row())
    def calculateSubTotal(self, ct, value):
        rate = float(self.table.item(0, 3).text())
        subtotal = value * rate

        item_subtotal = self.table.item(0, 4)
        if item_subtotal is None:
            item_subtotal = QtGui.QTableWidgetItem()
            self.table.setItem(0, 4, item_subtotal)

        item_subtotal.setText(str(subtotal))
if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    w.showMaximized()
    sys.exit(app.exec_())

from datetime import datetime
from PyQt4 import QtCore, QtGui
from functools import partial
import glob, os
global typ,showdetails,keypad
typ = "product"
from functools import partial
showdetails = False
import Tkinter as tk
global KeyBoardObj
global dialog
global cash_items
cash_items = True
global sidewidget
QSS = '''
QLabel#big{
    font: bold 30pt Comic Sans MS
}
QLabel#small{
    font: bold 18pt Comic Sans MS
}
QLabel#biggest{
    font: bold 35pt Comic Sans MS
}
'''
ROUNDED_STYLE_SHEET = """QPushButton {
     background-color: #062957;
     border: none;
     outline : None;
     color: white;
     width: 26px;
     padding: 12px 30px;
     text-align: center;
     text-decoration: none;
     font-size: 16px;
     margin: 4px 2px;
 }
"""
ROUNDED_STYLE_SHEET1 = """QPushButton {
     background-color: #062957;
     border: none;
     outline : None;
     color: white;
     width: 26px;
     padding: 12px 30px;
     text-align: center;
     text-decoration: none;
     font-size: 16px;
     margin: 4px 2px;
 }
"""
ROUNDED_STYLE_SHEET2 = """QPushButton {
     background-color: green;
     color: white;
     border: none;
     outline : None;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 23px;
     min-width: 18em;
     padding: 15px;
 }
"""
import threading
ROUNDED_STYLE_SHEET3 = """QPushButton {
     background-color: orange;
     color: white;
     border: none;
     outline : None;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 23px;
     min-width: 18em;
     padding: 15px;
 }
"""
ROUNDED_STYLE_SHEET4 = """QPushButton {
     background-color: blue;
     color: white;
     border: none;
     outline : None;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 23px;
     min-width: 18em;
     padding: 15px;
 }
"""
ROUNDED_STYLE_SHEET5 = """QPushButton {
     background-color: blue;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 25px;
     min-width: 8em;
     padding: 10px;
     border:none;outline : None;
 }
"""
ROUNDED_STYLE_SHEET6 = """QPushButton {
     background-color: red;
     color: white;
     border-style: outset;
     border-width: 4px;
     border:none;outline : None;
     border-radius: 15px;
     border-color: blue;
     font: bold 25px;
     min-width: 8em;
     padding: 10px;
 }
"""
ROUNDED_STYLE_SHEET7 = """QPushButton {
     background-color: brown;
     color: white;
     border-style: outset;
     border-width: 4px;
     border:none;outline : None;
     border-radius: 15px;
     border-color: red;
     font: bold 25px;
     min-width: 8em;
     padding: 10px;
 }
"""
import time
global settingobj,sidewidget
global count , pushBtnList,lineeditList
count = 0
pushBtnList = []
lineeditList=[]
global data_arraylist
data_arraylist={}
# import tkinter as tk

root = tk.Tk()
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
print screen_width,"widthhhhhhhhhhhhhhhhhh"
print screen_height,"heightttttttttt"
import json
class ThreadingClock(object):
    def __init__(self,interval=1):
        global sidewidget
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()
    def run(self):
        while True:
            try:
                sidewidget.displayTime()
            except:
                pass
            time.sleep(self.interval)
class Orders(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Orders, self).__init__(parent)
        global settingobj
        settingobj.dialog.close()
        self.scrollArea_left = QtGui.QScrollArea(widgetResizable=True)
        self.orders_widget = QtGui.QWidget()
        self.orders_widget.showFullScreen()
        self.orders_grid= QtGui.QGridLayout(self.orders_widget)
        self.qvw1 = QtGui.QWidget()
        self.online_order_hbox = QtGui.QVBoxLayout(self.qvw1)
        self.online_label = QtGui.QPushButton("Online Order")
        self.online_label.setStyleSheet("QPushButton{ background-color: #FF8C00; color: white;outline : None;}")
        self.online_order_hbox.addWidget(self.online_label,QtCore.Qt.AlignTop)
        self.orders_grid.addWidget(self.qvw1,0,0)
        self.qvw1.setFixedWidth(round((screen_width*33)/100))

        self.w1 = QtGui.QWidget()
        self.w1.mouseReleaseEvent=self.myfunction1
        self.w1_vbox=QtGui.QVBoxLayout(self.w1)
        self.date_label1 = QtGui.QLabel("Jan 24,2019           #175", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping = QtGui.QLabel("Shopping :online")
        self.ordercity = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label1)
        self.w1_vbox.addWidget(self.amount)
        self.w1_vbox.addWidget(self.shopping)
        self.w1_vbox.addWidget(self.ordercity)
        self.w1_vbox.addWidget(self.orderstate)
        self.online_order_hbox.addWidget(self.w1)


        self.w2 = QtGui.QWidget()
        self.w2.mouseReleaseEvent=self.myfunction1
        self.w2_vbox=QtGui.QVBoxLayout(self.w2)
        self.date_label2 = QtGui.QLabel("Jan 30,2019           #178", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount2 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping2 = QtGui.QLabel("Shopping :online")
        self.ordercity2 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate2 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label2)
        self.w1_vbox.addWidget(self.amount2)
        self.w1_vbox.addWidget(self.shopping2)
        self.w1_vbox.addWidget(self.ordercity2)
        self.w1_vbox.addWidget(self.orderstate2)
        # self.w1.resize(100,50)
        self.online_order_hbox.addWidget(self.w2)


        self.w3 = QtGui.QWidget()
        self.w3.mouseReleaseEvent=self.myfunction1
        self.w3_vbox=QtGui.QVBoxLayout(self.w3)
        self.date_label3 = QtGui.QLabel("Feb 2,2019           #179", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount3 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping3 = QtGui.QLabel("Shopping :online")
        self.ordercity3 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate3 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label3)
        self.w1_vbox.addWidget(self.amount3)
        self.w1_vbox.addWidget(self.shopping3)
        self.w1_vbox.addWidget(self.ordercity3)
        self.w1_vbox.addWidget(self.orderstate3)
        # self.w1.resize(100,50)
        self.online_order_hbox.addWidget(self.w3)


        self.w4 = QtGui.QWidget()
        self.w4.mouseReleaseEvent=self.myfunction1
        self.w4_vbox=QtGui.QVBoxLayout(self.w4)
        self.date_label4 = QtGui.QLabel("Feb 10,2019           #180", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount4 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping4 = QtGui.QLabel("Shopping :online")
        self.ordercity4 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate4 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label4)
        self.w1_vbox.addWidget(self.amount4)
        self.w1_vbox.addWidget(self.shopping4)
        self.w1_vbox.addWidget(self.ordercity4)
        self.w1_vbox.addWidget(self.orderstate4)
        # self.w1.resize(100,50)
        self.online_order_hbox.addWidget(self.w4)

        self.w5 = QtGui.QWidget()
        self.w5.mouseReleaseEvent=self.myfunction1
        self.w5_vbox=QtGui.QVBoxLayout(self.w5)
        self.date_label5 = QtGui.QLabel("Feb 25,2019           #181", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount5 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping5 = QtGui.QLabel("Shopping :online")
        self.ordercity5 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate5 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label5)
        self.w1_vbox.addWidget(self.amount5)
        self.w1_vbox.addWidget(self.shopping5)
        self.w1_vbox.addWidget(self.ordercity5)
        self.w1_vbox.addWidget(self.orderstate5)
        # self.w1.resize(100,50)
        self.online_order_hbox.addWidget(self.w5)



        self.online_order_hbox.addStretch()


        self.qvw2 = QtGui.QWidget()
        self.qvw2.setFixedWidth(round((screen_width*33)/100))
        self.offline_order_hbox = QtGui.QVBoxLayout(self.qvw2)
        self.orders_grid.addWidget(self.qvw2,0,3)

        self.offline_label = QtGui.QPushButton("Offline Order")
        self.offline_label.setStyleSheet("QPushButton{ background-color: blue; color: white;outline : None;}")
        self.offline_order_hbox.addWidget(self.offline_label,QtCore.Qt.AlignTop)



        self.w1 = QtGui.QWidget()
        self.w1.mouseReleaseEvent=self.myfunction1
        self.w1_vbox=QtGui.QVBoxLayout(self.w1)
        self.date_label1 = QtGui.QLabel("Jan 2,2019           #172", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping = QtGui.QLabel("Shopping :Offline")
        self.ordercity = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label1)
        self.w1_vbox.addWidget(self.amount)
        self.w1_vbox.addWidget(self.shopping)
        self.w1_vbox.addWidget(self.ordercity)
        self.w1_vbox.addWidget(self.orderstate)
        # self.w1.resize(100,50)
        self.offline_order_hbox.addWidget(self.w1)


        self.w2 = QtGui.QWidget()
        self.w2.mouseReleaseEvent=self.myfunction1
        self.w2_vbox=QtGui.QVBoxLayout(self.w2)
        self.date_label2 = QtGui.QLabel("Jan 15,2019           #170", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount2 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping2 = QtGui.QLabel("Shopping :Offline")
        self.ordercity2 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate2 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label2)
        self.w1_vbox.addWidget(self.amount2)
        self.w1_vbox.addWidget(self.shopping2)
        self.w1_vbox.addWidget(self.ordercity2)
        self.w1_vbox.addWidget(self.orderstate2)
        # self.w1.resize(100,50)
        self.offline_order_hbox.addWidget(self.w2)



        self.w3 = QtGui.QWidget()
        self.w3.mouseReleaseEvent=self.myfunction1
        self.w3_vbox=QtGui.QVBoxLayout(self.w3)
        self.date_label3 = QtGui.QLabel("Jan 26,2019           #169", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount3 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping3 = QtGui.QLabel("Shopping :Offline")
        self.ordercity3 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate3 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label3)
        self.w1_vbox.addWidget(self.amount3)
        self.w1_vbox.addWidget(self.shopping3)
        self.w1_vbox.addWidget(self.ordercity3)
        self.w1_vbox.addWidget(self.orderstate3)
        # self.w1.resize(100,50)
        self.offline_order_hbox.addWidget(self.w3)

        self.w4 = QtGui.QWidget()
        self.w4.mouseReleaseEvent=self.myfunction1
        self.w4_vbox=QtGui.QVBoxLayout(self.w4)
        self.date_label4 = QtGui.QLabel("Jan 28,2019           #165", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount4 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping4 = QtGui.QLabel("Shopping :Offline")
        self.ordercity4 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate4 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label4)
        self.w1_vbox.addWidget(self.amount4)
        self.w1_vbox.addWidget(self.shopping4)
        self.w1_vbox.addWidget(self.ordercity4)
        self.w1_vbox.addWidget(self.orderstate4)
        # self.w1.resize(100,50)
        self.offline_order_hbox.addWidget(self.w4)

        self.w5 = QtGui.QWidget()
        self.w5.mouseReleaseEvent=self.myfunction1
        self.w5_vbox=QtGui.QVBoxLayout(self.w5)
        self.date_label5 = QtGui.QLabel("Jan 29,2019           #163", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount5 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping5 = QtGui.QLabel("Shopping :Offline")
        self.ordercity5 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate5 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label5)
        self.w1_vbox.addWidget(self.amount5)
        self.w1_vbox.addWidget(self.shopping5)
        self.w1_vbox.addWidget(self.ordercity5)
        self.w1_vbox.addWidget(self.orderstate5)
        # self.w1.resize(100,50)
        self.offline_order_hbox.addWidget(self.w5)




        self.offline_order_hbox.addStretch()
        self.qvw3 = QtGui.QWidget()

        self.intime_progress_order_hbox = QtGui.QVBoxLayout(self.qvw3)
        self.qvw3.setFixedWidth(round((screen_width*33)/100))
        self.orders_grid.addWidget(self.qvw3,0,6)
        self.intime_progress_label = QtGui.QPushButton("In Progress")
        self.intime_progress_label.setStyleSheet("QPushButton{ background-color: green; color: white;outline : None;}")
        self.intime_progress_order_hbox.addWidget(self.intime_progress_label,QtCore.Qt.AlignTop)




        self.w1 = QtGui.QWidget()
        self.w1.mouseReleaseEvent=self.myfunction1
        self.w1_vbox=QtGui.QVBoxLayout(self.w1)
        self.date_label1 = QtGui.QLabel("Jan 4,2019           #150", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping = QtGui.QLabel("Shopping :In Progress")
        self.ordercity = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label1)
        self.w1_vbox.addWidget(self.amount)
        self.w1_vbox.addWidget(self.shopping)
        self.w1_vbox.addWidget(self.ordercity)
        self.w1_vbox.addWidget(self.orderstate)
        # self.w1.resize(100,50)
        self.intime_progress_order_hbox.addWidget(self.w1)

        self.w2 = QtGui.QWidget()
        self.w2.mouseReleaseEvent=self.myfunction1
        self.w2_vbox=QtGui.QVBoxLayout(self.w2)
        self.date_label2 = QtGui.QLabel("Jan 20,2019           #151", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount2 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping2 = QtGui.QLabel("Shopping :In Progress")
        self.ordercity2 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate2 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label2)
        self.w1_vbox.addWidget(self.amount2)
        self.w1_vbox.addWidget(self.shopping2)
        self.w1_vbox.addWidget(self.ordercity2)
        self.w1_vbox.addWidget(self.orderstate2)
        # self.w1.resize(100,50)
        self.intime_progress_order_hbox.addWidget(self.w2)


        self.w3 = QtGui.QWidget()
        self.w3.mouseReleaseEvent=self.myfunction1
        self.w3_vbox=QtGui.QVBoxLayout(self.w3)
        self.date_label3 = QtGui.QLabel("Jan 3,2019           #153", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount3 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping3 = QtGui.QLabel("Shopping :In Progress")
        self.ordercity3 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate3 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label3)
        self.w1_vbox.addWidget(self.amount3)
        self.w1_vbox.addWidget(self.shopping3)
        self.w1_vbox.addWidget(self.ordercity3)
        self.w1_vbox.addWidget(self.orderstate3)
        # self.w1.resize(100,50)
        self.intime_progress_order_hbox.addWidget(self.w3)

        self.w4 = QtGui.QWidget()
        self.w4.mouseReleaseEvent=self.myfunction1
        self.w4_vbox=QtGui.QVBoxLayout(self.w4)
        self.date_label4 = QtGui.QLabel("Jan 19,2019           #155", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount4 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping4 = QtGui.QLabel("Shopping :In Progress")
        self.ordercity4 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate4 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label4)
        self.w1_vbox.addWidget(self.amount4)
        self.w1_vbox.addWidget(self.shopping4)
        self.w1_vbox.addWidget(self.ordercity4)
        self.w1_vbox.addWidget(self.orderstate4)
        # self.w1.resize(100,50)
        self.intime_progress_order_hbox.addWidget(self.w4)

        self.w5 = QtGui.QWidget()
        self.w5.mouseReleaseEvent=self.myfunction1
        self.w5_vbox=QtGui.QVBoxLayout(self.w5)
        self.date_label5 = QtGui.QLabel("Jan 01,2019           #156", alignment= QtCore.Qt.AlignCenter,objectName="small")
        self.amount5 = QtGui.QLabel("Amount:"+u'\u20B9 191')
        self.shopping5 = QtGui.QLabel("Shopping :In Progress")
        self.ordercity5 = QtGui.QLabel("Order City :Hyderabad")
        self.orderstate5 = QtGui.QLabel("Order State :TELANGANA")
        self.w1_vbox.addWidget(self.date_label5)
        self.w1_vbox.addWidget(self.amount5)
        self.w1_vbox.addWidget(self.shopping5)
        self.w1_vbox.addWidget(self.ordercity5)
        self.w1_vbox.addWidget(self.orderstate5)
        # self.w1.resize(100,50)
        self.intime_progress_order_hbox.addWidget(self.w5)

        self.poslabel1 = QtGui.QPushButton("POS")
        self.intime_progress_order_hbox.addWidget(self.poslabel1)
        self.poslabel1.clicked.connect(self.tableProgram)

        self.poslabel1.setStyleSheet(ROUNDED_STYLE_SHEET7)
        self.Inventory = QtGui.QPushButton("Inventory")
        self.online_order_hbox.addWidget(self.Inventory)
        self.Inventory.setStyleSheet(ROUNDED_STYLE_SHEET6)
        self.scrollArea_left.setWidget(self.orders_widget)
        self.setCentralWidget(self.orders_widget)
    def tableProgram(self):
        settingobj.close()
        settingobj.dialog.pos_class.close()
        settingobj.mainProgram()
    def myfunction1(self,event):
        print "hello widget"
        self.widget_orders_info = QtGui.QWidget()
        self.order_info_vbox = QtGui.QVBoxLayout(self.widget_orders_info)
        self.orderdetail_label = QtGui.QLabel("Order Details(22HRS)",alignment =QtCore.Qt.AlignCenter,objectName="small")
        self.order_info_vbox.addWidget(self.orderdetail_label)
        self.line = QtGui.QFrame()
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.order_info_vbox.addWidget(self.line)
        self.order_hbox = QtGui.QHBoxLayout()
        self.vbox1 = QtGui.QVBoxLayout()
        self.datelabel = QtGui.QLabel("JAN 24 2019",alignment =QtCore.Qt.AlignCenter,objectName="small")
        self.statuslabel = QtGui.QLabel("Status: Created",alignment =QtCore.Qt.AlignCenter)
        self.amount = QtGui.QLabel("Amount:"+ u'\u20B9 14000',alignment =QtCore.Qt.AlignCenter)
        self.shoping = QtGui.QLabel("Shopping: online")
        self.shoping_charge = QtGui.QLabel("Shopping Charges:"+ u'\u20B9 0')
        self.paymentmode= QtGui.QLabel("Payment Mode:Card")
        self.padiamount = QtGui.QLabel(u'\u20B9 14000')
        self.phnbr= QtGui.QLabel('8885132435')
        self.vbox1.addWidget(self.datelabel)
        self.vbox1.addWidget(self.statuslabel)
        self.vbox1.addWidget(self.amount)
        self.vbox1.addWidget(self.shoping)
        self.vbox1.addWidget(self.shoping_charge)
        self.vbox1.addWidget(self.paymentmode)
        self.vbox1.addWidget(self.phnbr)
        self.order_hbox.addLayout(self.vbox1)
        self.line1 = QtGui.QFrame()
        self.line1.setFrameShape(QtGui.QFrame.VLine)
        self.order_hbox.addWidget(self.line1)

        self.vbox2 = QtGui.QVBoxLayout()
        self.item_label = QtGui.QLabel("Givers Gain(pack of 1) 1 Qty", objectName="big")
        self.vbox2.addWidget(self.item_label)
        self.hbox1 = QtGui.QHBoxLayout()
        self.price = QtGui.QLabel("Price :" + u'\u20B9 140')
        self.qnty =QtGui.QLabel("Quantity:100")
        self.discount = QtGui.QLabel("Discount:"+ u'\u20B9 0')
        self.totalprice =QtGui.QLabel("Total price:" +u'\u20B9 14000')
        self.paid = QtGui.QLabel("Paid amount :" +u'\u20B9 14000')
        self.hbox1.addWidget(self.price)
        self.hbox1.addWidget(self.qnty)
        self.hbox1.addWidget(self.discount)
        self.hbox1.addWidget(self.totalprice)
        self.hbox1.addWidget(self.paid)
        self.h2box = QtGui.QHBoxLayout()
        self.status1 = QtGui.QLabel("Status:Created")
        self.amount1 = QtGui.QLabel("Refund Amount:0")
        self.status2 = QtGui.QLabel("Refund Status")
        self.h2box.addWidget(self.status1)
        self.h2box.addWidget(self.amount1)
        self.h2box.addWidget(self.status2)
        self.vbox2.addLayout(self.hbox1)
        self.vbox2.addLayout(self.h2box)
        self.order_hbox.addLayout(self.vbox2)
        self.order_info_vbox.addLayout(self.order_hbox)
        self.widget_orders_info.show()
        self.widget_orders_info.resize(500,300)








class Table_Program(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Table_Program, self).__init__(parent)
        global sidewidget

        global settingobj,data_arraylist,count,pushBtnList,lineeditList
        settingobj.close()
        data_arraylist = {}
        self.fonttable = QtGui.QFont()
        self.fonttable.setPointSize(18)
        self.fonttable.setBold(True)
        try:
            with open("dictionary.txt","r") as f:
                js = json.load(f)
                data_arraylist = js
                print data_arraylist,type(data_arraylist)
        except:
            with open("dictionary.txt","w") as f:
                json.dump(data_arraylist,f)
                print 'file createddddddddd'
        self.scrollArea_left = QtGui.QScrollArea(widgetResizable=True)
        self.scrollArea_left.setStyleSheet('QScrollArea {border:none}')
        self.mainw2 = QtGui.QWidget()
        self.mainw2.showFullScreen()
        print  self.scrollArea_left.viewport().width(), "scroll areaaaaaaaaaaaaaaa width"

        self.scrollArea_left.setWidget(self.mainw2)
        self.mainw2.setStyleSheet("QWidget {background-color: black;color: white;}")
        self.newvbox = QtGui.QGridLayout(self.mainw2)

        self.table = QtGui.QTableWidget()
        self.table.setStyleSheet("QTableWidget{gridline-color: white}")
        self.table_item = QtGui.QTableWidgetItem()
        self.table.setRowCount(2)
        self.table.verticalHeader().hide()
        self.table.setColumnCount(6)
        self.table.setHorizontalHeaderLabels(("S.no, Item Description,Qty,Rate(Rs:),Subtotal,"",").split(','))
        self.serachhbox = QtGui.QHBoxLayout()
        self.labels = QtGui.QHBoxLayout()
        self.cioclabel = QtGui.QLabel("CIOC",objectName="big")

        self.poslabel = QtGui.QLabel("POS",objectName="big")

        self.poslabel.setAlignment(QtCore.Qt.AlignCenter)

        self.invoicelabel = QtGui.QLabel('Invoice Serial Number - 39',objectName="big")
        self.invoicelabel.setAlignment(QtCore.Qt.AlignRight)

        self.labels.addWidget(self.cioclabel)
        self.labels.addWidget(self.poslabel)
        self.labels.addWidget(self.invoicelabel)

        self.phnbr = QtGui.QLabel("Phone No:",objectName="small")
        self.searchlabel = QtGui.QPushButton("Search")
        self.searchlabel.clicked.connect(self.ph_number_enter)
        self.searchlabel.setStyleSheet("QPushButton{ background-color: blue; color: white;outline : None;}")
        self.searchEdit = QtGui.QLineEdit()
        self.textEdit = QtGui.QTextEdit()
        self.textEdit.setReadOnly(True)

        self.textEdit.setFixedSize(1320,150)


        self.serachhbox.addWidget(self.phnbr)

        self.serachhbox.addWidget(self.searchEdit)
        self.serachhbox.addWidget(self.searchlabel)
        self.newvbox.addLayout(self.labels,0,0)
        self.newvbox.addLayout(self.serachhbox,1,0)
        # self.newvbox.addWidget(self.textEdit,2,0)
        self.newvbox.addWidget(self.table,3,0)
        header = self.table.horizontalHeader()
        header.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Plain)
        header.setLineWidth(1)
        self.table.setHorizontalHeader(header)
        stylesheet = "::section{Background-color:black;color: white;font-size: 25px;}"
        self.table.horizontalHeader().setStyleSheet(stylesheet);
        header.setResizeMode(1, QtGui.QHeaderView.Stretch)
        header.setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(2, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(3, QtGui.QHeaderView.Stretch)
        header.setResizeMode(4, QtGui.QHeaderView.Stretch)
        vh = self.table.verticalHeader()
        vh.setDefaultSectionSize(50)
        self.btn1 = QtGui.QPushButton(icon=QtGui.QIcon("./plus1.png"))
        self.btn1.setIconSize(QtCore.QSize(60,30))
        self.btn1.setStyleSheet("QPushButton {outline : None;}")
        self.table.setCellWidget(1,0,self.btn1)
        self.btn1.clicked.connect(self.insert_rows)
        self.table.setItem(0,0,QtGui.QTableWidgetItem(str(1)))
        self.table.setItem(1,4,QtGui.QTableWidgetItem(str(" ")))
        self.table.setFont(self.fonttable)
        self.btn = QtGui.QPushButton(icon=QtGui.QIcon("./delete.png"))
        self.btn.setIconSize(QtCore.QSize(40,60))
        self.table.setCellWidget(0,5,self.btn)
        self.itemlineedit = LineEdititems_list()
        lineeditList.append(self.itemlineedit)
        self.items_listKB = KeyBoard('six')
        self.create_connections('six')
        self.quantybutton = QtGui.QLineEdit()
        pushBtnList.append(self.quantybutton)
        index = QtCore.QPersistentModelIndex(self.table.model().index(0, 5))
        self.btn.clicked.connect(lambda: self.handleButton1(index))
        self.table.setCellWidget(0, 2,self.quantybutton)
        self.table.setCellWidget(0, 1,self.itemlineedit)
        self.quantybutton.textEdited.connect(lambda:self.quantity(index))
        self.hbox2 = QtGui.QHBoxLayout()
        self.btn2 = QtGui.QPushButton("Inventory")
        self.hbox2.addWidget(self.btn2)
        self.btn2.setStyleSheet(ROUNDED_STYLE_SHEET6)
        self.btn3 = QtGui.QPushButton(" Reset Form")
        self.btn3.setStyleSheet(ROUNDED_STYLE_SHEET7)
        self.hbox2.addWidget(self.btn3)
        self.hbox2.addStretch()
        self.orderbtn4 = QtGui.QPushButton("Orders")
        self.orderbtn4.clicked.connect(self.pos_orders)
        self.orderbtn4.setStyleSheet(ROUNDED_STYLE_SHEET7)
        self.hbox2.addWidget(self.orderbtn4)
        self.hbox2.addStretch()
        self.paybtn4 = QtGui.QPushButton("Pay")
        self.paybtn4.clicked.connect(self.cards_selection)
        self.paybtn4.setStyleSheet(ROUNDED_STYLE_SHEET5)
        self.paybtn4.setIcon(QtGui.QIcon("money.png"))
        self.paybtn4.setIconSize(QtCore.QSize(20, 30))
        self.hbox2.addWidget(self.paybtn4)
        self.newvbox.addLayout(self.hbox2,4,0)

        self.hbox = QtGui.QHBoxLayout()
        sidewidget=Widget()

        self.w =QtGui.QWidget()
        self.w.setStyleSheet("QWidget {background-color: black;color: white;}")
        self.total_vbox = QtGui.QVBoxLayout(self.w)

        hbox =QtGui.QHBoxLayout()
        hbox.addWidget(self.scrollArea_left)
        hbox.addWidget(sidewidget)
        self.total_vbox.addLayout(hbox)
        self.setCentralWidget(self.w)

    def pos_orders(self):
        self.pos_class = Orders(self)
        # self.pos_class.show()
        self.pos_class.showFullScreen()
    def ph_number_enter(self):
        global settingobj,dialog
        text_key =self.searchEdit.text()
        self.textEdit.clear()
        self.searchitems(text_key)


    def keyPressEvent(self, event):
        global cash_items
        cash_items = True
        try:
            if event.key() == QtCore.Qt.Key_Return:
                text_key =self.searchEdit.text()
                self.textEdit.clear()
                self.searchitems(text_key)
            elif  cash_items == True:

                sidewidget.cash_return_text = int(sidewidget.cash_tender.text()) - int(self.final_cost)
                sidewidget.cash_return.setText(str(sidewidget.cash_return_text))
        except:
            pass
    def orders_data(self):
            pass
    def searchitems(self,text_key):
        self.newvbox.addWidget(self.textEdit,2,0)
        self.textEdit.clear()
        global data_arraylist
        if str(text_key) in  data_arraylist:
            ph_data = data_arraylist[str(text_key)]
            self.textEdit.append(str("Name").ljust(15," ")   +":"+"\t\t"+str(ph_data["Name"]))
            self.textEdit.append(str("Address").ljust(14," ")   +":"+"\t\t"+str(ph_data["Address"]))
            self.textEdit.append(str("Pincode").ljust(14," ")   +":"+"\t\t"+str(ph_data["Pincode"]))
            self.textEdit.append(str("City").ljust(19," ")   +":"+"\t\t"+str(ph_data["City"]))
            self.textEdit.append(str("State").ljust(17," ")   +":"+"\t\t"+str(ph_data["State"]))

        else:
            self.textEdit.clear()

            self.dialogerror = QtGui.QDialog()
            self.errorvbox = QtGui.QVBoxLayout(self.dialogerror)
            self.labeleroor = QtGui.QLabel("Your phone number is not found please enter your details")
            self.ok = QtGui.QPushButton("OK")
            self.ok.clicked.connect(self.users_details)
            self.errorvbox.addWidget(self.labeleroor)
            self.errorvbox.addWidget(self.ok)
            self.dialogerror.setGeometry(500,650,300,100)
            self.dialogerror.exec_()
    def insert_rows(self):
        if len(self.quantybutton.text())==0:
            self.message_dialog = QtGui.QDialog()
            self.msgvbox = QtGui.QVBoxLayout(self.message_dialog)
            self.msglabel = QtGui.QLabel("PLEASE ADD ITEMS")
            self.ok = QtGui.QPushButton("OK")
            self.ok.clicked.connect(self.message_dialog.close)
            self.msgvbox.addWidget(self.msglabel)
            self.msgvbox.addWidget(self.ok)
            self.message_dialog.setGeometry(300,500,100,100)
            self.message_dialog.exec_()
        else:
            ct = self.table.rowCount()
            self.table.insertRow(ct-1)
            self.btn = QtGui.QPushButton()
            self.btn.setStyleSheet("QPushButton{ background-color: black; color: black;outline : None;}")
            self.btn.setIcon(QtGui.QIcon("./delete.png"))
            self.btn.setIconSize(QtCore.QSize(40,60))
            self.table.setItem(ct-1,0,QtGui.QTableWidgetItem(str(ct)))
            self.table.setCellWidget(ct -1,5,self.btn)
            index = QtCore.QPersistentModelIndex(self.table.model().index(ct -1, 5))
            self.btn.clicked.connect(lambda: self.handleButton1(index))
            self.quantybutton = QtGui.QLineEdit()

            pushBtnList.append(self.quantybutton)
            self.table.setCellWidget(ct -1, 2, self.quantybutton)
            self.quantybutton.textChanged.connect(lambda: self.quantity(index))

            self.itemlineedit = LineEdititems_list()
            self.items_listKB = KeyBoard('six')
            self.create_connections('six')
            lineeditList.append(self.itemlineedit)
            self.table.setCellWidget(ct -1, 1, self.itemlineedit)



    def users_details(self):
        self.dialogerror.close()
        self.d = QtGui.QDialog()
        self.detailsvbox = QtGui.QVBoxLayout(self.d)
        self.form = QtGui.QFormLayout()

        self.namelabel = QtGui.QLabel("Name:")
        self.addresslabel = QtGui.QLabel("Address:")
        self.citylabel = QtGui.QLabel("City:")
        self.statelabel = QtGui.QLabel("State:")
        self.pincodelabel = QtGui.QLabel("Pincode:")

        self.nameedit = LineEditname()
        self.addressedit= LineEditaddress()
        self.cityedit = LineEditcity()
        self.stateedit = LineEditstate()
        self.pincodeedit = LineEditpincode()
        self.detailsok = QtGui.QPushButton("OK")
        self.detailsok.clicked.connect(self.append_list)

        self.form.addRow(self.namelabel,self.nameedit)
        self.form.addRow(self.addresslabel, self.addressedit)
        self.form.addRow(self.citylabel,self.cityedit)
        self.form.addRow(self.statelabel,self.stateedit)
        self.form.addRow(self.pincodelabel,self.pincodeedit)
        self.detailsvbox.addLayout(self.form)
        self.detailsvbox.addWidget(self.detailsok)
        self.d.setGeometry(500, 650, 300, 100)
        self.d.show()
        self.kb = KeyBoard('fst')
        self.create_connections('fst')
        self.kb1 = KeyBoard('sec')
        self.create_connections('sec')
        self.kb2 = KeyBoard('thr')
        self.create_connections('thr')
        self.kb3 = KeyBoard('four')
        self.create_connections('four')
        self.pincode = NumberKeyBoard('pincodenumber',self)
        self.connect_numeric_keyboard('pincodenumber')
    def create_connections(self,typ):
        print typ
        if typ == 'fst':
            self.nameedit.signal_evoke_kb.connect(partial(self.show_kb,typ))
        elif typ == 'sec':
            self.addressedit.signal_evoke_kb1.connect(partial(self.show_kb,typ))
        elif typ == 'thr':
            self.cityedit.signal_evoke_kb2.connect(partial(self.show_kb,typ))
        elif typ == 'four':
            self.stateedit.signal_evoke_kb3.connect(partial(self.show_kb,typ))

        elif typ == 'six':
            self.itemlineedit.signal_evoke_items_listKB .connect(partial(self.show_kb,typ))

    def show_kb(self,typ):
        print 'kb typ',typ
        global KeyBoardObj
        try:
            KeyBoardObj.close()
            print 'old keyboard objjjjjjjjjjjj Deleted'
        except:
            print 'no keyboard objjjjjjjjjjjj'

        if typ == 'fst':
            KeyBoardObj = self.kb
            if self.kb.isHidden():
                self.kb.show()
            else:
                self.kb.hide()
        elif typ == 'sec':
            KeyBoardObj = self.kb1
            if self.kb1.isHidden():
                self.kb1.show()
            else:
                self.kb1.hide()
        elif typ == 'thr':
            KeyBoardObj = self.kb2
            if self.kb2.isHidden():
                self.kb2.show()
            else:
                self.kb2.hide()
        elif typ == 'four':
            KeyBoardObj = self.kb3
            if self.kb3.isHidden():
                self.kb3.show()
            else:
                self.kb3.hide()

        elif typ == 'six':
            KeyBoardObj = self.items_listKB
            if self.items_listKB.isHidden():
                self.items_listKB.show()
            else:
                self.items_listKB.hide()

    def append_list(self):
        global data_arraylist
        self.d.close()
        self.name = self.nameedit.text()
        self.address = self.addressedit.text()
        self.city = self.cityedit.text()
        self.state = self.stateedit.text()
        self.pincode =self.pincodeedit.text()
        phone_number = self.searchEdit.text()
        data_arraylist[str(phone_number)] = {"Name": str(self.name),"Address" : str(self.address),"City":str(self.city),"State":str(self.state),"Pincode":str(self.pincode)}
        print "after updated", data_arraylist
        with open("dictionary.txt","w") as f:
            json.dump(data_arraylist,f)
    def myfunction2(self,event):
        print "hellooooo second"
    def myfunction1(self,event):
        # print " hello ",self.l1
        global data_arraylist
        txt = str(self.l1.text())
        self.dialog1 = QtGui.QDialog()
        self.layout = QtGui.QVBoxLayout(self.dialog1)
        self.orderphlabel = QtGui.QLabel(txt + " " + self.PhoneNumber)
        self.orderpersonname = QtGui.QLabel("Name : Gowthami")

        self.layout.addWidget(self.orderphlabel)
        self.layout.addWidget(self.orderpersonname)
        for i in range(len(data_arraylist[str(self.PhoneNumber)])):
            x="items:  " +data_arraylist[str(self.PhoneNumber)][i]['item1']
            self.desitem1 = QtGui.QLabel(x)
        self.layout.addWidget(self.desitem1)
        self.printbtn = QtGui.QPushButton("Print")
        self.layout.addWidget(self.printbtn)
        self.dialog1.setGeometry(500,600,300,300)
        self.dialog1.exec_()
    def new_data(self):
        print "yessssssssssssssssssssssssssssssssss"
        Table_Program()

    def connect_numeric_keyboard(self,typ):
        print typ,'numberic keyboard typppppppppp'
        if typ == 'pincodenumber':
            self.pincodeedit.signal_evoke_kb4.connect(partial(self.show_numericKB,typ))


    def show_numericKB(self,typ):
        global KeyBoardObj
        print typ,'numberic keyboard typppppppppp'
        try:
            KeyBoardObj.close()
            print 'old keyboard objjjjjjjjjjjj Deleted'
        except:
            print 'no keyboard objjjjjjjjjjjj'

        if typ == 'pincodenumber':
            KeyBoardObj = self.pincode
            if self.pincode.isHidden():
                self.pincode.show()

            else:
                self.pincode.hide()
    def quantity(self,index):
        global count,pushBtnList
        print "index", index.row()
        s=index.row()
        count  = int(pushBtnList[index.row()].text())
        print "count is", count
        # text_quantity = str(self.quantybutton.text())
        # print "valueeee", text_quantity,type(text_quantity)
        rate = float(self.table.item(s, 3).text())
        subtotal = int(str(count)) * rate
        print "sss", self.table.rowCount()
        new_total_list = []
        for row in range(self.table.rowCount()- 1):
            q_count = int(pushBtnList[row].text())
            rate_count = float(self.table.item(row, 3).text())
            grand_total = int(str(q_count)) * rate_count
            new_total_list.append(grand_total)
            self.final_cost = sum(new_total_list)
            total_cost = "Total " + u"\u20B9" + " : "+str(self.final_cost)



            sidewidget.total_cost_label.setText(total_cost)
            sidewidget.totalsidevbox.addWidget(sidewidget.total_cost_label,5,0)

        self.table.setItem(self.table.rowCount()- 1, 4, QtGui.QTableWidgetItem(str(sum(new_total_list))))
        item_subtotal = self.table.item(s, 4)
        if item_subtotal is None:
            item_subtotal = QtGui.QTableWidgetItem()
            self.table.setItem(s, 4, item_subtotal)
        item_subtotal.setText(str(subtotal))




    def handleButton1(self, index):
        print('button clicked:', index.row())
        global pushBtnList
        r = index.row()
        if index.isValid():
            self.table.removeRow(r)
            del pushBtnList[r]
            del lineeditList[r]
            print('button clicked after:', r,self.table.rowCount())
            for i in range(r,self.table.rowCount()-1):
                print i,i+1
                self.table.setItem(i,0,QtGui.QTableWidgetItem(str(i+1)))
    def cards_selection(self):

        sidewidget.totalsidevbox.addLayout(sidewidget.cards_hbox,8,0)
        sidewidget.totalsidevbox.addLayout(sidewidget.cash_deliver_hbox,7,0)

        verticalSpacer = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        sidewidget.totalsidevbox.addItem(verticalSpacer, 8, 0, QtCore.Qt.AlignTop)

    def keyBoardClicked1(self,name):
        if name == '<--':
            self.lineedit.backspace()
        else:
            txt = str(self.lineedit.text()) + str(name)
            self.lineedit.setText(txt)

    def checkLogin(self):
            text = self.eline.text()
            if text == "":
                self.msg = QtGui.QMessageBox()
                self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
                self.msg.setText( ("Please Enter Valid Email") )
                self.msg.setWindowTitle( ("Email Not Valid") )
                self.msg.setStandardButtons(QtGui.QMessageBox.Ok)
                self.msg.exec_()
            elif self.passline.text() == "":
                self.msg = QtGui.QMessageBox()
                self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
                self.msg.setText( ("Please Enter Password") )
                self.msg.setWindowTitle( ("Password Not Valid") )
                self.msg.setStandardButtons(QtGui.QMessageBox.Retry)
                self.msg.exec_()
            else:
                self.newwidget = QtGui.QWidget()
                self.grid = QtGui.QGridLayout(self.newwidget)
                self.adlbl = QtGui.QLabel( ("Admin") )
                self.logoutBtn = QtGui.QPushButton( ("Logout") )
                self.logoutBtn.setStyleSheet("QPushButton{ background-color: #c43700; color: white;  padding: 14px 20px;margin: 8px 0; border: none;  width: 100%;}")
                self.logoutBtn.clicked.connect(self.showLogin)
                self.vbox = QtGui.QVBoxLayout()
                self.vbox.addWidget(self.logoutBtn)
                self.grid.addWidget(self.iconBtn,0,1,QtCore.Qt.AlignCenter)
                self.grid.addWidget(self.adlbl,1,1,QtCore.Qt.AlignCenter)
                self.grid.addLayout(self.vbox,2,1,QtCore.Qt.AlignCenter)
                self.grid.setAlignment(QtCore.Qt.AlignCenter)
                self.scrollArea.setWidget(self.newwidget)
class KeyPad(QtGui.QWidget):
    def __init__(self, parent=None):
        super(KeyPad, self).__init__(parent)
        grid_lay = QtGui.QGridLayout(self)
        global settingobj,dialog
        keys =  [
            ("7", QtCore.Qt.Key_7 , 0, 0, 1, 1),
            ("8", QtCore.Qt.Key_8 , 0, 1, 1, 1),
            ("9", QtCore.Qt.Key_9 , 0, 2, 1, 1),
            ("-", QtCore.Qt.Key_Minus , 0, 3, 1, 1),
            ("4", QtCore.Qt.Key_4 , 1, 0, 1, 1),
            ("5", QtCore.Qt.Key_5 , 1, 1, 1, 1),
            ("6", QtCore.Qt.Key_6 , 1, 2, 1, 1),
            ("+", QtCore.Qt.Key_Plus , 1, 3, 1, 1),
            ("1", QtCore.Qt.Key_1 , 2, 0, 1, 1),
            ("2", QtCore.Qt.Key_2 , 2, 1, 1, 1),
            ("3", QtCore.Qt.Key_3 , 2, 2, 1, 1),
            ("0", QtCore.Qt.Key_0 , 3, 0, 1, 2),
            (".", QtCore.Qt.Key_Period , 3, 2, 1, 1),
            ("Enter", QtCore.Qt.Key_Return , 2, 3, 2, 1)
        ]

        for text, key, r, c, sr, sc in keys:

            button = QtGui.QPushButton(text=text, focusPolicy=QtCore.Qt.NoFocus)
            button.setProperty("_key_", key)
            grid_lay.addWidget(button, r, c, sr, sc)
            button.clicked.connect(self.on_clicked)
            button.setMinimumWidth(100)
            button.setMinimumHeight(100)
            button.setStyleSheet("QPushButton { outline : None;background-color: blue;color:white;font-size: 25px}")
            if text == "Enter":
                sp = button.sizePolicy()
                sp.setVerticalPolicy(sp.horizontalPolicy())
                button.setSizePolicy(sp)



    @QtCore.pyqtSlot()
    def on_clicked(self):
        global cash_items
        cash_items = True
        button = self.sender()
        text = "" if button.text() == "enter" else button.text()
        key = button.property("_key_")
        widget = QtGui.QApplication.focusWidget()
        if hasattr(key, 'toPyObject'):
            key = key.toPyObject()
        if widget:
            event = QtGui.QKeyEvent(QtCore.QEvent.KeyPress, key, QtCore.Qt.NoModifier, text)
            QtCore.QCoreApplication.postEvent(widget, event)
        if cash_items == True:
            sidewidget.cash_tender.setText(sidewidget.cash_tender.text() +text)

class Widget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(Widget, self).__init__(parent)
        global settingobj,dialog

        cash_btn = QtGui.QPushButton("Cash", clicked=self.on_cash_btn_clicked)
        cash_btn.setIcon(QtGui.QIcon(QtGui.QPixmap("./cash.png")))
        cash_btn.setIconSize(QtCore.QSize(35,40))
        cash_btn.setStyleSheet("QPushButton { outline : None;background-color: green;color:white;font-size: 25px}")
        card_btn = QtGui.QPushButton("Card",clicked=self.on_card_btn_clicked)
        card_btn.setIconSize(QtCore.QSize(35,40))
        card_btn.setIcon(QtGui.QIcon(QtGui.QPixmap("./cards.png")))
        card_btn.setStyleSheet("QPushButton { outline : None;background-color: 	#FF8C00;color:white;font-size: 25px}")
        wallet_btn = QtGui.QPushButton("Wallet",clicked=self.on_wallet_btn_clicked)
        wallet_btn.setIcon(QtGui.QIcon(QtGui.QPixmap("./wallet.png")))
        wallet_btn.setIconSize(QtCore.QSize(35,40))
        wallet_btn.setStyleSheet("QPushButton { outline : None;background-color: blue;color:white;font-size: 25px}")

        self.cash_widget = QtGui.QWidget(visible=False)
        self.cash_tender = QtGui.QLabel(objectName="big")

        self.cash_return = QtGui.QLabel(objectName="big")

        self.card_widget = QtGui.QWidget(visible=False)
        self.card_widgetvbox = QtGui.QVBoxLayout(self.card_widget)

        self.info_label = QtGui.QLabel("Please Use Swipe Machine",objectName="small")
        self.card_widgetvbox.addWidget(self.info_label)



        self.wallet_widget = QtGui.QWidget(visible=False)
        self.wallet_pictures = QtGui.QVBoxLayout(self.wallet_widget)
        self.wallet_hbox = QtGui.QHBoxLayout()

        self.googlepaylabel1 = QtGui.QPushButton()
        self.googlepaylabel1.setIcon(QtGui.QIcon("./googlepay.png"))
        self.googlepaylabel1.clicked.connect(self.googlepay_image)
        self.googlepaylabel1.setStyleSheet("QPushButton { outline : None;}")
        self.googlepaylabel1.setIconSize(QtCore.QSize(150,50))

        self.paytmlabel2 = QtGui.QPushButton()
        self.paytmlabel2.setIcon(QtGui.QIcon("./paytm.jpeg"))
        self.paytmlabel2.clicked.connect(self.paytm_image)
        self.paytmlabel2.setStyleSheet("QPushButton { outline : None;}")
        self.paytmlabel2.setIconSize(QtCore.QSize(150,50))


        self.phonepay = QtGui.QPushButton()
        self.phonepay.setIcon(QtGui.QIcon("./phonepay.png"))
        self.phonepay.clicked.connect(self.phonepay_image)
        self.phonepay.setStyleSheet("QPushButton { outline : None;}")
        self.phonepay.setIconSize(QtCore.QSize(150,50))
        self.wallet_hbox.addWidget(self.googlepaylabel1)
        self.wallet_hbox.addWidget(self.paytmlabel2)
        self.wallet_hbox.addWidget(self.phonepay)
        self.wallet_pictures.addLayout(self.wallet_hbox)


        self.cash_tender_label = QtGui.QLabel("Received Amount " +  u'\u20B9 :',objectName="small")

        self.cash_return_label =QtGui.QLabel("Return Amount  " +  u'\u20B9   :',objectName="small")
        self.cash_formlayout = QtGui.QFormLayout()
        self.cash_formlayout.setLabelAlignment(QtCore.Qt.AlignCenter)

        self.cash_formlayout.addRow(self.cash_tender_label,self.cash_tender)

        self.cash_formlayout.addRow(self.cash_return_label,self.cash_return)


        cash_lay = QtGui.QVBoxLayout(self.cash_widget)
        cash_lay.addLayout(self.cash_formlayout)


        keypad = KeyPad()

        self.cards_hbox = QtGui.QHBoxLayout()
        self.cards_hbox.addWidget(cash_btn)
        self.cards_hbox.addWidget(card_btn)
        self.cards_hbox.addWidget(wallet_btn)

        self.cash_deliver_hbox = QtGui.QHBoxLayout()
        self.cash_deliver  = QtGui.QPushButton("Cash Delivery")
        self.cash_deliver.setStyleSheet("QPushButton{ background-color: blue; color: white;outline : None;}")
        self.cash_pay_later  = QtGui.QPushButton("Pay Later")
        self.cash_pay_later.setStyleSheet("QPushButton{ background-color: red; color: white;outline : None;}")
        self.cash_deliver_hbox.addWidget(self.cash_deliver,0, QtCore.Qt.AlignLeft)
        self.cash_deliver_hbox.addWidget(self.cash_pay_later,0, QtCore.Qt.AlignRight)


        self.timelabel = QtGui.QLabel(alignment = QtCore.Qt.AlignTop,objectName="small")

        self.totalsidevbox = QtGui.QGridLayout(self)#setLayout
        self.shiftle = QtGui.QLabel('SHIFT 3',alignment = QtCore.Qt.AlignTop,objectName="small")


        self.machinenumber_box = QtGui.QHBoxLayout()
        self.machine_number = QtGui.QLabel("03",alignment = QtCore.Qt.AlignRight,objectName="big")
        self.machinenumber_box.addWidget(self.timelabel)
        self.machinenumber_box.addWidget(self.machine_number)

        self.totalsidevbox.addLayout(self.machinenumber_box,0,0)
        self.logouthbox = QtGui.QHBoxLayout()
        self.logout_btn = QtGui.QPushButton()
        self.logout_btn.clicked.connect(self.main_window)
        self.logout_btn.setIcon(QtGui.QIcon(QtGui.QPixmap("./power1.png")))
        self.logouthbox.addWidget(self.shiftle)
        self.logouthbox.addWidget(self.logout_btn,0, QtCore.Qt.AlignRight)

        self.logout_btn.setIconSize(QtCore.QSize(35,40))
        self.totalsidevbox.addLayout(self.logouthbox,1,0)
        self.line2 = QtGui.QFrame()
        self.line2.setFrameShape(QtGui.QFrame.HLine)
        self.totalsidevbox.addWidget(self.line2,2,0)
        self.akramle = QtGui.QLabel('AKRAM',objectName="small")
        self.line4 = QtGui.QFrame()
        self.line4.setFrameShape(QtGui.QFrame.HLine)
        self.totalsidevbox.addWidget(self.akramle,3,0)
        self.totalsidevbox.addWidget(self.line4,4,0)
        self.line5 = QtGui.QFrame()
        self.line5.setFrameShape(QtGui.QFrame.HLine)

        self.total_cost_label = QtGui.QLabel(alignment = QtCore.Qt.AlignCenter,objectName="biggest")


        self.totalsidevbox.addWidget(self.line5,6,0)

        self.totalsidevbox.addWidget(self.cash_widget,9,0)
        self.totalsidevbox.addWidget(self.card_widget,9,0)
        self.totalsidevbox.addWidget(self.wallet_widget,9,0)
        verticalSpacer = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.totalsidevbox.addItem(verticalSpacer, 4, 0, QtCore.Qt.AlignTop)
        self.totalsidevbox.addWidget(keypad,60,0,QtCore.Qt.AlignBottom)
        self.setFixedWidth(550)
    def googlepay_image(self):
        self.googlepay_widget = QtGui.QWidget()
        self.googlepay_vbox =  QtGui.QVBoxLayout(self.googlepay_widget)
        self.googlepay_label = QtGui.QLabel()
        self.googlepay_vbox.addWidget(self.googlepay_label)
        pixmap1 =QtGui.QPixmap("./googlepay.png")
        pixmap1= pixmap1.scaledToWidth(500)
        self.googlepay_label.setPixmap(pixmap1)
        self.googlepay_widget.setGeometry(300,400,300,300)
        self.googlepay_widget.show()
    def paytm_image(self):
        self.paytm_widget = QtGui.QWidget()
        self.paytm_vbox =  QtGui.QVBoxLayout(self.paytm_widget)
        self.paytm_label = QtGui.QLabel()
        self.paytm_vbox.addWidget(self.paytm_label)
        pixmap2 =QtGui.QPixmap("./paytm.jpeg")
        pixmap2= pixmap2.scaledToWidth(500)
        self.paytm_label.setPixmap(pixmap2)
        self.paytm_widget.setGeometry(300,400,300,300)
        self.paytm_widget.show()
    def phonepay_image(self):
        self.phonepay_widget = QtGui.QWidget()
        self.phonepay_vbox =  QtGui.QVBoxLayout(self.phonepay_widget)
        self.phonepay_label = QtGui.QLabel()
        self.phonepay_vbox.addWidget(self.phonepay_label)
        pixmap2 =QtGui.QPixmap("./phonepay.png")
        pixmap2= pixmap2.scaledToWidth(500)
        self.phonepay_label.setPixmap(pixmap2)
        self.phonepay_widget.setGeometry(300,400,300,300)
        self.phonepay_widget.show()
    def main_window(self):
        global settingobj
        settingobj.show()
    def displayTime(self):
        now = datetime.now()
        time =now.strftime('%H:%M')
        # cur_time = datetime.strftime(datetime.now(), "%I:%M:%S %p ")
        self.timelabel.setText(time)


    def on_cash_btn_clicked(self):
        global cash_items
        cash_items = True
        keypad = KeyPad()
        self.cash_widget.setVisible(not self.cash_widget.isVisible())
        if self.cash_widget.isVisible():
            self.cash_widget.setFocus()
            self.cash_tender.clear()
            self.cash_return.setText(" ")

    def on_card_btn_clicked(self):
        self.card_widget.setVisible(not self.card_widget.isVisible())
        if self.card_widget.isVisible():
            self.card_widget.setFocus()


    def on_wallet_btn_clicked(self):
        self.wallet_widget.setVisible(not self.wallet_widget.isVisible())
        if self.wallet_widget.isVisible():
            self.wallet_widget.setFocus()

class LineEdititems_list(QtGui.QLineEdit):
    signal_evoke_items_listKB =  QtCore.pyqtSignal()
    def __init__(self):
        super(LineEdititems_list, self).__init__()
    def mousePressEvent(self, QMouseEvent):
        super(LineEdititems_list, self).mousePressEvent(QMouseEvent)
        self.signal_evoke_items_listKB.emit()

class LineEditname(QtGui.QLineEdit):
    signal_evoke_kb =  QtCore.pyqtSignal()
    def __init__(self):
        super(LineEditname, self).__init__()
    def mousePressEvent(self, QMouseEvent):
        super(LineEditname, self).mousePressEvent(QMouseEvent)
        self.signal_evoke_kb.emit()

class LineEditaddress(QtGui.QLineEdit):
    signal_evoke_kb1 =  QtCore.pyqtSignal()
    def __init__(self):
        super(LineEditaddress, self).__init__()
    def mousePressEvent(self, QMouseEvent):
        super(LineEditaddress, self).mousePressEvent(QMouseEvent)
        self.signal_evoke_kb1.emit()

class LineEditcity(QtGui.QLineEdit):
    signal_evoke_kb2 =  QtCore.pyqtSignal()
    def __init__(self):
        super(LineEditcity, self).__init__()
    def mousePressEvent(self, QMouseEvent):
        super(LineEditcity, self).mousePressEvent(QMouseEvent)
        self.signal_evoke_kb2.emit()

class LineEditstate(QtGui.QLineEdit):
    signal_evoke_kb3 =  QtCore.pyqtSignal()
    def __init__(self):
        super(LineEditstate, self).__init__()
    def mousePressEvent(self, QMouseEvent):
        super(LineEditstate, self).mousePressEvent(QMouseEvent)
        self.signal_evoke_kb3.emit()

class LineEditpincode(QtGui.QLineEdit):
    signal_evoke_kb4 =  QtCore.pyqtSignal()
    def __init__(self):
        super(LineEditpincode, self).__init__()
    def mousePressEvent(self, QMouseEvent):
        super(LineEditpincode, self).mousePressEvent(QMouseEvent)
        self.signal_evoke_kb4.emit()
class NumberKeyBoard(QtGui.QWidget):

    def __init__(self,typ,cObj = ''):
        print typ,'numberic typ'
        self.typ = typ
        self.cObj = cObj
        super(NumberKeyBoard, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)
        self.vBox = QtGui.QVBoxLayout()
        self.flayout = QtGui.QHBoxLayout()
        self.btnlayout = QtGui.QHBoxLayout()
        custom_signal = QtCore.pyqtSignal()
        self.setStyleSheet("QWidget {background-color:black;color:white}")
        group5 = QtGui.QButtonGroup(self)
        for i in range(0,10):
            self.accBtn = QtGui.QPushButton(str(i))
            self.accBtn.clicked.connect(partial(self.keyBoardClicked1,i,self.typ))
            self.flayout.addWidget(self.accBtn)
            group5.addButton(self.accBtn)
            self.flayout.setAlignment(QtCore.Qt.AlignBottom)
            self.accBtn.setStyleSheet(ROUNDED_STYLE_SHEET)



        self.btn = QtGui.QPushButton(("Backspace"))

        self.btn.setStyleSheet(ROUNDED_STYLE_SHEET)
        self.btn.clicked.connect(partial(self.backspace1,self.typ))
        self.btnlayout.addWidget(self.btn)


        self.btn2 = QtGui.QPushButton(("Close"))

        self.btn2.setStyleSheet(ROUNDED_STYLE_SHEET)
        self.btn2.clicked.connect(self.close)
        self.btnlayout.addWidget(self.btn2)
        self.btnlayout.setAlignment(QtCore.Qt.AlignBottom)

        self.vBox.addLayout(self.flayout)
        self.vBox.addLayout(self.btnlayout)
        self.vBox.setAlignment(QtCore.Qt.AlignBottom)
        self.setLayout(self.vBox)
        # self.setGeometry(250,500,100,50)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)

    def backspace1(self,typ):
        if typ == 'pincodenumber':
            settingobj.dialog.pincodeedit.backspace()

    def keyBoardClicked1(self,key,typ='s'):
        print key,typ
        if typ == 'pincodenumber':
            txt = str(settingobj.dialog.pincodeedit.text()) + str(key)
            settingobj.dialog.pincodeedit.setText(txt)


class KeyBoard(QtGui.QWidget):

    def __init__(self,typ,cObj = ''):


        self.typ = typ
        self.cObj = cObj
        print cObj,'rrrrrrrrrr'
        super(KeyBoard, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)


        self.vBox = QtGui.QVBoxLayout()
        self.flayout = QtGui.QHBoxLayout()
        self.slayout = QtGui.QHBoxLayout()
        self.tlayout = QtGui.QHBoxLayout()
        self.folayout = QtGui.QHBoxLayout()
        self.btnlayout = QtGui.QHBoxLayout()
        custom_signal = QtCore.pyqtSignal()
        self.setStyleSheet("QWidget {background-color:black;color:white}")
        group3 = QtGui.QButtonGroup(self)
        for key in [  'q','w','e','r','t','y','u','i','o','p']:
            self.accBtn = QtGui.QPushButton(str(key))
            self.accBtn.clicked.connect(partial(self.keyBoardClicked,key,self.typ))
            self.slayout.addWidget(self.accBtn)
            self.slayout.setAlignment(QtCore.Qt.AlignBottom)
            self.accBtn.setStyleSheet(ROUNDED_STYLE_SHEET)

        for key in [ 'a','s','d','f','g','h','j','k','l','@']:
            self.accBtn = QtGui.QPushButton(str(key))
            self.accBtn.clicked.connect(partial(self.keyBoardClicked,key,self.typ))
            self.tlayout.addWidget(self.accBtn)
            self.tlayout.setAlignment(QtCore.Qt.AlignBottom)
            self.accBtn.setStyleSheet(ROUNDED_STYLE_SHEET)
        for key in [ 'z','x','c','v','b','n','m',',','.','.csv']:
            self.accBtn = QtGui.QPushButton(str(key))
            self.accBtn.clicked.connect(partial(self.keyBoardClicked,key,self.typ))
            self.folayout.addWidget(self.accBtn)
            self.folayout.setAlignment(QtCore.Qt.AlignBottom)
            self.accBtn.setStyleSheet(ROUNDED_STYLE_SHEET)


        self.btn = QtGui.QPushButton("Backspace")

        self.btn.clicked.connect(partial(self.backspace,self.typ))
        self.btnlayout.addWidget(self.btn)
        self.btn.setStyleSheet(ROUNDED_STYLE_SHEET)


        self.btn1 = QtGui.QPushButton("Enter")

        self.btn1.setStyleSheet(ROUNDED_STYLE_SHEET)
        # self.btn1.clicked.connect(partial(self.enter,self.typ))

        self.btnlayout.addWidget(self.btn1)


        self.btn2 = QtGui.QPushButton("Close")

        self.btn2.setStyleSheet(ROUNDED_STYLE_SHEET)
        self.btn2.clicked.connect(self.close)

        self.btnlayout.addWidget(self.btn2)


        self.btn3 = QtGui.QPushButton("Spacebar")

        self.btn3.setStyleSheet(ROUNDED_STYLE_SHEET)

        self.btnlayout.addWidget(self.btn3)
        self.btn3.clicked.connect(partial(self.keyBoardClicked,'Spacebar',self.typ))

        # self.vBox.addLayout(self.flayout)

        self.vBox.addLayout(self.slayout)

        self.vBox.addLayout(self.tlayout)
        self.vBox.addLayout(self.folayout)
        self.vBox.addLayout(self.btnlayout)
        self.setLayout(self.vBox)
        self.setGeometry(280,500,320,250)
        self.vBox.setAlignment(QtCore.Qt.AlignBottom)
        # self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
    def backspace(self,typ):
        if typ == 'fst':
            settingobj.dialog.nameedit.backspace()
        elif typ == 'sec':
            settingobj.dialog.addressedit.backspace()
        elif typ == 'thr':
            ettingobj.dialog.cityedit.backspace()
        elif typ == 'four':
            settingobj.dialog.stateedit.backspace()
        # elif typ == 'five':
        #     settingobj.dialog.pincodeedit.backspace()
        elif typ == 'six':
            settingobj.dialog.itemlineedit.backspace()

    def keyBoardClicked(self,key,typ='s'):
        if typ == 'fst':
            if str(key) == 'Spacebar':
                txt = str(settingobj.dialog.nameedit.text()) + ' '
            else:
                txt = str(settingobj.dialog.nameedit.text()) + str(key)
            settingobj.dialog.nameedit.setText(txt)
        elif typ == 'sec':
            if str(key) == 'Spacebar':
                txt = str(settingobj.dialog.addressedit.text()) + ' '
            else:
                txt = str(settingobj.dialog.addressedit.text()) + str(key)
            settingobj.dialog.addressedit.setText(txt)
        elif typ == 'thr':
            if str(key) == 'Spacebar':
                txt = str(settingobj.dialog.cityedit.text()) + ' '
            else:
                txt = str(settingobj.dialog.cityedit.text()) + str(key)
            settingobj.dialog.cityedit.setText(txt)
        elif typ == 'four':
            if str(key) == 'Spacebar':
                txt = str(settingobj.dialog.stateedit.text()) + ' '
            else:
                txt = str(settingobj.dialog.stateedit.text()) + str(key)
            settingobj.dialog.stateedit.setText(txt)
        # elif typ == 'five':
        #     if str(key) == 'Spacebar':
        #         txt = str(settingobj.dialog.pincodeedit.text()) + ' '
        #     else:
        #         txt = str(settingobj.dialog.pincodeedit.text()) + str(key)
        #     settingobj.dialog.pincodeedit.setText(txt)
        elif typ == 'six':
            if str(key) == 'Spacebar':
                txt = str(settingobj.dialog.itemlineedit.text()) + ' '
            else:
                txt = str(settingobj.dialog.itemlineedit.text()) + str(key)
            settingobj.dialog.itemlineedit.setText(txt)


class Settings(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Settings, self).__init__(parent)
        self.newwidget = QtGui.QWidget()
        global width, height
        self.mainLayout = QtGui.QHBoxLayout(self.newwidget)
        self.setCentralWidget(QtGui.QWidget(self))
        self.centralWidget().setLayout(self.mainLayout)
        self.showMaximized()
        self.eline = QtGui.QLineEdit()
        self.eline.setText("123")
        self.eline.setPlaceholderText( ("Username/Email/Mobile") )
        self.eline.setFixedWidth(250)
        self.passline = QtGui.QLineEdit()
        self.passline.setText("123")
        self.passline.setPlaceholderText( ("Enter Password") )
        self.passline.setFixedWidth(250)
        self.passline.setEchoMode(QtGui.QLineEdit.Password)
        self.loginlabel = QtGui.QLabel("Login")

        self.loginlabel.setStyleSheet("QLabel {font-size: 30px;}")
        self.loginBtn = QtGui.QPushButton("Sign in")
        self.loginBtn.clicked.connect(self.showDock)
        self.loginBtn.setStyleSheet("QPushButton{ background-color: #2a1b82; color: white; border: none; width: 5;height:30;}")
        self.hbox = QtGui.QHBoxLayout()
        self.imagelabel = QtGui.QLabel()
        self.imagelabel.resize(2200,1060)
        pixmap = QtGui.QPixmap("./frontimage1.png").scaled(self.imagelabel.size(), QtCore.Qt.KeepAspectRatio,QtCore.Qt.SmoothTransformation)
        self.imagelabel.setPixmap(pixmap)
        self.hbox.addWidget(self.imagelabel)
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.loginlabel)
        self.vbox.addWidget(self.eline)
        self.vbox.addWidget(self.passline)
        self.vbox.addWidget(self.loginBtn)
        self.vbox.setAlignment(QtCore.Qt.AlignCenter)
        self.mainLayout.addLayout(self.hbox)
        self.mainLayout.addLayout(self.vbox)
    def showDock(self):
        text = self.eline.text()
        text1 = self.passline.text()
        if text == "123"  and text1 == "123":
            self.mainProgram()
        elif text == "" or text1 == "" or text != "123" or text1 != "123":
            self.msg = QtGui.QMessageBox()
            self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
            self.msg.setText( ("Please Enter Valid Email") )
            self.msg.setWindowTitle( ("Email Not Valid") )
            self.msg.setStandardButtons(QtGui.QMessageBox.Retry)
            self.msg.exec_()
        else:
            pass
    def mainProgram(self):
        self.dialog = Table_Program(self)
        self.dialog.showFullScreen()
if __name__ == '__main__':
    import sys
    global settingobj
    clockThreading = ThreadingClock()

    app = QtGui.QApplication(sys.argv)
    app.setStyleSheet(QSS)
    app.setQuitOnLastWindowClosed(False)
    settingobj= Settings()
    settingobj.show()
    settingobj.showFullScreen()
    sys.exit(app.exec_())

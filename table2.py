from datetime import datetime
from PyQt4 import QtCore, QtGui
from functools import partial
import glob, os
global typ,showdetails
typ = "product"
from functools import partial
showdetails = False
import Tkinter as tk
global KeyBoardObj
# root = Tkinter.Tk()
# width = root.winfo_screenwidth()
# height = root.winfo_screenheight()

ROUNDED_STYLE_SHEET1 = """QPushButton {
     background-color: #062957;
     border: none;
     outline : None;
     color: white;
     width: 26px;
     padding: 12px 30px;
     text-align: center;
     text-decoration: none;
     font-size: 16px;
     margin: 4px 2px;
 }
"""
ROUNDED_STYLE_SHEET2 = """QPushButton {
     background-color: green;
     color: white;
     border: none;
     outline : None;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 23px;
     min-width: 18em;
     padding: 15px;
 }
"""

ROUNDED_STYLE_SHEET3 = """QPushButton {
     background-color: orange;
     color: white;
     border: none;
     outline : None;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 23px;
     min-width: 18em;
     padding: 15px;
 }
"""
ROUNDED_STYLE_SHEET4 = """QPushButton {
     background-color: blue;
     color: white;
     border: none;
     outline : None;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 23px;
     min-width: 18em;
     padding: 15px;
 }
"""
ROUNDED_STYLE_SHEET5 = """QPushButton {
     background-color: blue;
     color: white;
     border-style: outset;
     border-width: 4px;
     border-radius: 15px;
     border-color: none;
     font: bold 20px;
     min-width: 8em;
     padding: 10px;
     border:none;outline : None;
 }
"""
ROUNDED_STYLE_SHEET6 = """QPushButton {
     background-color: red;
     color: white;
     border-style: outset;
     border-width: 4px;
     border:none;outline : None;
     border-radius: 15px;
     border-color: blue;
     font: bold 17px;
     min-width: 8em;
     padding: 10px;
 }
"""
ROUNDED_STYLE_SHEET7 = """QPushButton {
     background-color: brown;
     color: white;
     border-style: outset;
     border-width: 4px;
     border:none;outline : None;
     border-radius: 15px;
     border-color: red;
     font: bold 17px;
     min-width: 8em;
     padding: 10px;
 }
"""
QSS = '''
QLabel#big{
    font: bold 20pt Comic Sans MS
}
QLabel#small{
    font: bold 15pt Comic Sans MS
}
'''
global settingobj
global count , pushBtnList
count = 0
pushBtnList = []

global data_arraylist
data_arraylist={}
# import tkinter as tk

root = tk.Tk()
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
print screen_width,"widthhhhhhhhhhhhhhhhhh"
print screen_height,"heightttttttttt"
import json
class Table_Program(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Table_Program, self).__init__(parent)

        global settingobj,data_arraylist,count,pushBtnList
        settingobj.close()
        data_arraylist = {}
        try:
            with open("dictionary.txt","r") as f:
                js = json.load(f)
                data_arraylist = js
                print data_arraylist,type(data_arraylist)
        except:
            with open("dictionary.txt","w") as f:
                json.dump(data_arraylist,f)
                print 'file createddddddddd'
        self.scrollArea_left = QtGui.QScrollArea(widgetResizable=True)

        self.mainw2 = QtGui.QWidget()
        self.mainw2.showFullScreen()
        self.totalvbox = QtGui.QVBoxLayout(self.mainw2)
        self.w = QtGui.QWidget()
        self.scrollArea_left.setWidget(self.w)

        self.mainw2.setStyleSheet("QWidget {background-color: black;color: white;}")

        self.newvbox = QtGui.QVBoxLayout(self.w)
        self.table = QtGui.QTableWidget()
        self.table.setStyleSheet("QTableWidget{gridline-color: white}")
        self.table_item = QtGui.QTableWidgetItem()
        self.table.setRowCount(2)
        self.table.verticalHeader().hide()
        self.table.setColumnCount(6)
        self.table.setHorizontalHeaderLabels(("S.no, Item Description,Qty,Rate(Rs:),Subtotal,"",").split(','))
        self.serachhbox = QtGui.QHBoxLayout()
        self.labels = QtGui.QHBoxLayout()
        self.cioclabel = QtGui.QLabel("CIOC")

        self.poslabel = QtGui.QLabel("POS")
        self.poslabel.setAlignment(QtCore.Qt.AlignCenter)

        self.invoicelabel = QtGui.QLabel('Invoice Serial Number - 39')
        self.invoicelabel.setAlignment(QtCore.Qt.AlignRight)

        self.labels.addWidget(self.cioclabel)
        self.labels.addWidget(self.poslabel)
        self.labels.addWidget(self.invoicelabel)

        self.phnbr = QtGui.QLabel("Phone No:")
        self.searchlabel = QtGui.QLabel("Search")
        self.searchEdit = LineEdit()
        self.textEdit = QtGui.QTextEdit()

        self.textEdit.setFixedSize(1860,150)
        self.searchkb = NumberKeyBoard('searchedit',self)
        self.connect_numeric_keyboard('searchedit')

        self.serachhbox.addWidget(self.phnbr)
        self.serachhbox.addWidget(self.searchEdit)
        self.serachhbox.addWidget(self.searchlabel)
        self.newvbox.addLayout(self.labels)
        self.newvbox.addLayout(self.serachhbox)
        self.newvbox.addWidget(self.textEdit)
        self.newvbox.addWidget(self.table)
        header = self.table.horizontalHeader()
        header.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Plain)
        header.setLineWidth(1)
        self.table.setHorizontalHeader(header)
        stylesheet = "::section{Background-color:black;color: white}"
        self.table.horizontalHeader().setStyleSheet(stylesheet);
        header.setResizeMode(1, QtGui.QHeaderView.Stretch)
        header.setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(2, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(3, QtGui.QHeaderView.Stretch)
        header.setResizeMode(4, QtGui.QHeaderView.Stretch)
        vh = self.table.verticalHeader()
        vh.setDefaultSectionSize(50)
        self.btn1 = QtGui.QPushButton(icon=QtGui.QIcon("./plus1.png"))
        self.btn1.setIconSize(QtCore.QSize(60,30))
        self.btn1.setStyleSheet("QPushButton {outline : None;}")
        self.table.setCellWidget(1,0,self.btn1)
        self.btn1.clicked.connect(self.insert_rows)
        self.table.setItem(0,0,QtGui.QTableWidgetItem(str(1)))
        self.btn = QtGui.QPushButton(icon=QtGui.QIcon("./delete.png"))
        self.btn.setIconSize(QtCore.QSize(40,60))
        self.table.setCellWidget(0,5,self.btn)
        self.quantybutton = QtGui.QPushButton(str(count))
        pushBtnList.append(self.quantybutton)
        index = QtCore.QPersistentModelIndex(self.table.model().index(0, 5))
        self.btn.clicked.connect(lambda: self.handleButton1(index))
        self.table.setCellWidget(0, 2,self.quantybutton)
        self.quantybutton.clicked.connect(lambda:self.quantity(index))
        self.hbox2 = QtGui.QHBoxLayout()
        self.btn2 = QtGui.QPushButton("Inventory")
        self.hbox2.addWidget(self.btn2)
        self.btn2.setStyleSheet(ROUNDED_STYLE_SHEET6)
        self.btn3 = QtGui.QPushButton(" Reset Form")
        self.btn3.setStyleSheet(ROUNDED_STYLE_SHEET7)
        self.hbox2.addWidget(self.btn3)
        self.hbox2.addStretch()
        self.paybtn4 = QtGui.QPushButton("Pay")
        self.paybtn4.clicked.connect(self.cards_selection)
        self.paybtn4.setStyleSheet(ROUNDED_STYLE_SHEET5)
        self.paybtn4.setIcon(QtGui.QIcon("money.png"))
        self.paybtn4.setIconSize(QtCore.QSize(20, 30))
        self.hbox2.addWidget(self.paybtn4)
        self.newvbox.addLayout(self.hbox2)

        self.hbox = QtGui.QHBoxLayout()


        self.timelabel = QtGui.QLabel()
        self.timer = QtCore.QTimer(self)
        self.font = QtGui.QFont()
        self.font.setPointSize(18)
        self.font.setBold(True)
        self.timelabel.setFont(self.font)

        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.displayTime)
        self.timer.start()
        self.orderbtn = QtGui.QPushButton("Orders")
        self.newbtn = QtGui.QPushButton("New")
        self.newbtn.clicked.connect(self.new_data)
        self.hbox.addWidget(self.timelabel)
        self.hbox.addWidget(self.orderbtn)
        self.hbox.addWidget(self.newbtn)
        self.w.showFullScreen()

        self.totalvbox.addWidget(self.scrollArea_left)
        self.totalvbox.addLayout(self.hbox)

        self.setCentralWidget(self.mainw2)
    def connect_numeric_keyboard(self,typ):
        # print typ,'numberic keyboard typppppppppp'
        if typ == 'searchedit':
            self.searchEdit.signal_evoke_kb.connect(partial(self.show_numericKB,typ))


    def show_numericKB(self,typ):
        global KeyBoardObj
        try:
            KeyBoardObj.close()
            print 'old keyboard objjjjjjjjjjjj Deleted'
        except:
            print 'no keyboard objjjjjjjjjjjj'

        if typ == 'searchedit':
            KeyBoardObj = self.searchkb
            if self.searchkb.isHidden():
                self.searchkb.show()
            else:
                self.searchkb.hide()

    def displayTime(self):
        cur_time = datetime.strftime(datetime.now(), "%I:%M:%S %p ")
        self.timelabel.setText(cur_time)
    def keyPressEvent(self, event):
        # print event,event.key(),QtCore.Qt.Key_Return
        if event.key() == QtCore.Qt.Key_Return:
            text_key =self.searchEdit.text()
            self.searchitems(text_key)
    def orders_data(self):
            pass
    def searchitems(self,text_key):
        global data_arraylist
        if str(text_key) in  data_arraylist:
            ph_data = data_arraylist[str(text_key)]
            for key, value in sorted(ph_data.items()):
                self.textEdit.append(str(key)   +"\t\t"+" : " +"\t"+ value)
                self.textEdit.setFont(self.font)
        else:
            self.textEdit.clear()
            self.dialogerror = QtGui.QDialog()
            self.errorvbox = QtGui.QVBoxLayout(self.dialogerror)
            self.labeleroor = QtGui.QLabel("Your phone number is not found please enter your details")
            self.ok = QtGui.QPushButton("OK")
            self.ok.clicked.connect(self.users_details)
            self.errorvbox.addWidget(self.labeleroor)
            self.errorvbox.addWidget(self.ok)
            self.dialogerror.setGeometry(500,650,300,100)
            self.dialogerror.exec_()
    def insert_rows(self):
        ct = self.table.rowCount()
        self.table.insertRow(ct-1)
        self.btn = QtGui.QPushButton()
        self.btn.setStyleSheet("QPushButton{ background-color: black; color: black;outline : None;}")
        self.btn.setIcon(QtGui.QIcon("./delete.png"))
        self.btn.setIconSize(QtCore.QSize(40,60))
        self.table.setItem(ct-1,0,QtGui.QTableWidgetItem(str(ct)))
        self.table.setCellWidget(ct -1,5,self.btn)
        index = QtCore.QPersistentModelIndex(self.table.model().index(ct -1, 5))
        self.btn.clicked.connect(lambda: self.handleButton1(index))
        self.quantybutton = QtGui.QPushButton("0")
        pushBtnList.append(self.quantybutton)
        self.table.setCellWidget(ct -1, 2, self.quantybutton)
        self.quantybutton.clicked.connect(lambda: self.quantity(index))
    def users_details(self):
        self.dialogerror.close()
        self.d = QtGui.QDialog()
        self.detailsvbox = QtGui.QVBoxLayout(self.d)
        self.form = QtGui.QFormLayout()

        self.namelabel = QtGui.QLabel("Name:")
        self.addresslabel = QtGui.QLabel("Address:")
        self.citylabel = QtGui.QLabel("City:")
        self.statelabel = QtGui.QLabel("State:")
        self.pincodelabel = QtGui.QLabel("Pincode:")

        self.nameedit = QtGui.QLineEdit()
        self.addressedit= QtGui.QLineEdit()
        self.cityedit = QtGui.QLineEdit()
        self.stateedit = QtGui.QLineEdit()
        self.pincodeedit = QtGui.QLineEdit()
        self.detailsok = QtGui.QPushButton("OK")
        self.detailsok.clicked.connect(self.append_list)

        self.form.addRow(self.namelabel,self.nameedit)
        self.form.addRow(self.addresslabel, self.addressedit)
        self.form.addRow(self.citylabel,self.cityedit)
        self.form.addRow(self.statelabel,self.stateedit)
        self.form.addRow(self.pincodelabel,self.pincodeedit)
        self.detailsvbox.addLayout(self.form)
        self.detailsvbox.addWidget(self.detailsok)
        self.d.setGeometry(500, 650, 300, 100)
        self.d.show()

    def append_list(self):
        global data_arraylist
        self.d.close()
        self.name = self.nameedit.text()
        self.address = self.addressedit.text()
        self.city = self.cityedit.text()
        self.state = self.stateedit.text()
        self.pincode =self.pincodeedit.text()
        phone_number = self.searchEdit.text()
        data_arraylist[str(phone_number)] = {"Name": str(self.name),"Address" : str(self.address),"City":str(self.city),"State":str(self.state),"Pincode":str(self.pincode)}
        print "after updated", data_arraylist
        with open("dictionary.txt","w") as f:
            json.dump(data_arraylist,f)
    def myfunction2(self,event):
        print "hellooooo second"
    def myfunction1(self,event):
        # print " hello ",self.l1
        global data_arraylist
        txt = str(self.l1.text())
        self.dialog1 = QtGui.QDialog()
        self.layout = QtGui.QVBoxLayout(self.dialog1)
        self.orderphlabel = QtGui.QLabel(txt + " " + self.PhoneNumber)
        self.orderpersonname = QtGui.QLabel("Name : Gowthami")

        self.layout.addWidget(self.orderphlabel)
        self.layout.addWidget(self.orderpersonname)
        for i in range(len(data_arraylist[str(self.PhoneNumber)])):
            x="items:  " +data_arraylist[str(self.PhoneNumber)][i]['item1']
            self.desitem1 = QtGui.QLabel(x)
        self.layout.addWidget(self.desitem1)
        self.printbtn = QtGui.QPushButton("Print")
        self.layout.addWidget(self.printbtn)
        self.dialog1.setGeometry(500,600,300,300)
        self.dialog1.exec_()
    def new_data(self):
        print "new dataaaaaaaaaaa"
    def connect_numeric_keyboard(self,typ):
        print typ,'numberic keyboard typppppppppp'
        if typ == 'searchedit':
            self.searchEdit.signal_evoke_kb.connect(partial(self.show_numericKB,typ))


    def show_numericKB(self,typ):
        global KeyBoardObj
        try:
            KeyBoardObj.close()
            print 'old keyboard objjjjjjjjjjjj Deleted'
        except:
            print 'no keyboard objjjjjjjjjjjj'

        if typ == 'searchedit':
            KeyBoardObj = self.searchkb
            if self.searchkb.isHidden():
                self.searchkb.show()
            else:
                self.searchkb.hide()
    def quantity(self,index):
        global count,pushBtnList
        # print int(pushBtnList[index.row()].text()),'row texttttttttttt',index.row()
        count = int(pushBtnList[index.row()].text())
        print count
        self.items_quantity = QtGui.QWidget()
        self.hquantityhbox = QtGui.QHBoxLayout(self.items_quantity)
        self.increment = QtGui.QPushButton("+")
        self.increment.clicked.connect(partial(self.increment_order,index.row()))
        self.decrement = QtGui.QPushButton("-")
        self.decrement.clicked.connect(partial(self.decrement_order,index.row()))
        self.hquantityhbox.addWidget(self.increment)
        self.hquantityhbox.addWidget(self.decrement)
        self.items_quantity.setGeometry(300,300,100,50)
        self.items_quantity.show()
    def increment_order(self,s):
        global count
        count += 1
        pushBtnList[s].setText(str(count))
        rate = float(self.table.item(s, 3).text())
        subtotal = count * rate
        print subtotal, "subtotal"
        item_subtotal = self.table.item(s, 4)
        if item_subtotal is None:
            item_subtotal = QtGui.QTableWidgetItem()
            self.table.setItem(s, 4, item_subtotal)
        item_subtotal.setText(str(subtotal))

    def decrement_order(self,s,x):
        global  count
        count -=1
        if count <=0:
            pushBtnList[s].setText(str(0))
        else:

            pushBtnList[s].setText(str(count))
        rate = float(self.table.item(s, 3).text())
        subtotal = x * rate

        item_subtotal = self.table.item(s, 4)
        if item_subtotal is None:
            item_subtotal = QtGui.QTableWidgetItem()
            self.table.setItem(s, 4, item_subtotal)

        item_subtotal.setText(str(subtotal))



    def handleButton1(self, index):
        print('button clicked:', index.row())
        global pushBtnList
        r = index.row()
        if index.isValid():
            self.table.removeRow(r)
            del pushBtnList[r]
            print('button clicked after:', r,self.table.rowCount())
            for i in range(r,self.table.rowCount()-1):
                print i,i+1
                self.table.setItem(i,0,QtGui.QTableWidgetItem(str(i+1)))
    def cards_selection(self):
        self.paywidget = QtGui.QWidget()
        grid = QtGui.QGridLayout(self.paywidget)
        label_payment = QtGui.QLabel("Payment Details",alignment= QtCore.Qt.AlignTop)
        label_received = QtGui.QLabel("Received Amount")
        label_payment.setFont(self.font)
        label_received.setFont(self.font)

        label_amount = QtGui.QLabel("Amount: 192",alignment= QtCore.Qt.AlignRight)
        label_quantity = QtGui.QLabel("Quantity: 1", alignment= QtCore.Qt.AlignRight)
        label_amount.setFont(self.font)
        label_quantity.setFont(self.font)

        vbox1 = QtGui.QVBoxLayout(spacing=0)
        vbox1.setContentsMargins(0, 0, 0, 0)
        vbox1.addWidget(label_amount)
        vbox1.addWidget(label_quantity)

        cash_button = QtGui.QPushButton("Cash")
        cash_button.setFont(self.font)
        card_button = QtGui.QPushButton("Card")
        card_button.setFont(self.font)
        wallet_button = QtGui.QPushButton("Wallet")
        wallet_button.setFont(self.font)
        self.savevbox = QtGui.QVBoxLayout()
        self.savebtn = QtGui.QPushButton("Save")
        self.savebtn.setFont(self.font)
        self.savevbox.addWidget(self.savebtn,QtCore.Qt.AlignCenter)


        self.lineedit = QtGui.QLineEdit()
        self.lineedit.setFont(self.font)
        self.lineedit.setStyleSheet("border: 1px solid black;")
        sp = self.lineedit.sizePolicy()
        sp.setHorizontalPolicy(QtGui.QSizePolicy.Fixed)
        self.lineedit.setSizePolicy(sp)

        grid_buttons = QtGui.QGridLayout()
        grid_buttons.setSpacing(0)


        self.vbox = QtGui.QVBoxLayout()
        self.vbox.setSpacing(4)
        self.vbox.addStretch(1)
        names = [['7', '8', '9'],
                ['4', '5', '6'],
                ['1', '2', '3'],
                ['<--', '0', '.']]


        for pos in names:
            self.hbox = QtGui.QHBoxLayout()
            self.hbox.addStretch(1)
            self.hbox.setSpacing(5)
            for name in pos:

                button = QtGui.QPushButton(name)
                button.setMinimumSize(120,60)
                button.setMaximumSize(120,60)
                button.setFont(self.font)
                button.clicked.connect(partial(self.keyBoardClicked1,name))
                self.hbox.addWidget(button)
            self.vbox.addLayout(self.hbox)



        grid.addWidget(label_payment, 0, 0, 2, 3)
        grid.addLayout(vbox1, 0, 5)
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        grid.addWidget(line, 1,0,1,6)

        hbox = QtGui.QHBoxLayout(spacing=0)
        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.addWidget(cash_button)
        hbox.addWidget(card_button)
        hbox.addWidget(wallet_button)

        grid.addLayout(hbox, 2, 0, 1, 6)
        vbox = QtGui.QVBoxLayout(spacing=0)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(label_received)
        vbox.addWidget(self.lineedit)
        grid.addLayout(vbox, 3, 0)

        grid.addLayout(self.vbox, 3, 3, 3, 3)
        grid.addLayout(self.savevbox,6,2,QtCore.Qt.AlignCenter)
        self.paywidget.setGeometry(300,300,750,450)


        self.paywidget.show()

    def keyBoardClicked1(self,name):
        if name == '<--':
            self.lineedit.backspace()
        else:
            txt = str(self.lineedit.text()) + str(name)
            self.lineedit.setText(txt)

    def checkLogin(self):
            text = self.eline.text()
            if text == "":
                self.msg = QtGui.QMessageBox()
                self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
                self.msg.setText( ("Please Enter Valid Email") )
                self.msg.setWindowTitle( ("Email Not Valid") )
                self.msg.setStandardButtons(QtGui.QMessageBox.Ok)
                self.msg.exec_()
            elif self.passline.text() == "":
                self.msg = QtGui.QMessageBox()
                self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
                self.msg.setText( ("Please Enter Password") )
                self.msg.setWindowTitle( ("Password Not Valid") )
                self.msg.setStandardButtons(QtGui.QMessageBox.Retry)
                self.msg.exec_()
            else:
                self.newwidget = QtGui.QWidget()
                self.grid = QtGui.QGridLayout(self.newwidget)
                self.adlbl = QtGui.QLabel( ("Admin") )
                self.logoutBtn = QtGui.QPushButton( ("Logout") )
                self.logoutBtn.setStyleSheet("QPushButton{ background-color: #c43700; color: white;  padding: 14px 20px;margin: 8px 0; border: none;  width: 100%;}")
                self.logoutBtn.clicked.connect(self.showLogin)
                self.vbox = QtGui.QVBoxLayout()
                self.vbox.addWidget(self.logoutBtn)
                self.grid.addWidget(self.iconBtn,0,1,QtCore.Qt.AlignCenter)
                self.grid.addWidget(self.adlbl,1,1,QtCore.Qt.AlignCenter)
                self.grid.addLayout(self.vbox,2,1,QtCore.Qt.AlignCenter)
                self.grid.setAlignment(QtCore.Qt.AlignCenter)
                self.scrollArea.setWidget(self.newwidget)
class LineEdit(QtGui.QLineEdit):
    signal_evoke_kb = QtCore.pyqtSignal()
    def __init__(self):
        super(LineEdit, self).__init__()
    def mousePressEvent(self, QMouseEvent):
        super(LineEdit, self).mousePressEvent(QMouseEvent)
        self.signal_evoke_kb.emit()

class NumberKeyBoard(QtGui.QWidget):

    def __init__(self,typ,cObj = ''):
        print typ,'numberic typ'
        self.typ = typ
        self.cObj = cObj
        super(NumberKeyBoard, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)
        self.vBox = QtGui.QVBoxLayout()
        self.flayout = QtGui.QHBoxLayout()
        self.btnlayout = QtGui.QHBoxLayout()
        custom_signal =  QtCore.pyqtSignal()
        self.setStyleSheet("QWidget {background-color:black;color:white}")
        group5 = QtGui.QButtonGroup(self)
        for i in range(0,10):
            self.accBtn = QtGui.QPushButton(str(i))
            self.accBtn.clicked.connect(partial(self.keyBoardClicked,i,self.typ))
            self.flayout.addWidget(self.accBtn)
            self.accBtn.setStyleSheet(ROUNDED_STYLE_SHEET1)
            group5.addButton(self.accBtn)
            self.flayout.setAlignment(QtCore.Qt.AlignBottom)
        self.btn = QtGui.QPushButton(("Backspace"))
        self.btn.setStyleSheet(ROUNDED_STYLE_SHEET1)
        self.btn.clicked.connect(partial(self.backspace,self.typ))
        self.btnlayout.addWidget(self.btn)
        self.btn2 = QtGui.QPushButton(("Close"))
        self.btn2.setStyleSheet(ROUNDED_STYLE_SHEET1)
        self.btn2.clicked.connect(self.close)
        self.btnlayout.addWidget(self.btn2)


        self.btn3 = QtGui.QPushButton("Enter")
        self.btn3.setStyleSheet(ROUNDED_STYLE_SHEET1)
        self.btn3.clicked.connect(partial(self.Enter,self.typ))
        self.btnlayout.addWidget(self.btn3)

        self.btnlayout.setAlignment(QtCore.Qt.AlignBottom)
        self.vBox.addLayout(self.flayout)
        self.vBox.addLayout(self.btnlayout)
        self.vBox.setAlignment(QtCore.Qt.AlignBottom)
        self.setLayout(self.vBox)
        self.setGeometry(250,500,100,100)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
    def Enter(self,typ):
        if typ == 'searchedit':
            text_key =  settingobj.dialog.searchEdit.text()
            settingobj.dialog.searchitems(text_key)
    def keyBoardClicked(self,key,typ='s'):
        print key,typ
        if typ == 'searchedit':

            txt = str(settingobj.dialog.searchEdit.text())+str(key)

            settingobj.dialog.searchEdit.setText(txt)
    def backspace(self,typ):
        if typ == 'searchedit':
            settingobj.dialog.searchEdit.backspace()

class Settings(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Settings, self).__init__(parent)
        self.newwidget = QtGui.QWidget()
        global width, height
        self.mainLayout = QtGui.QHBoxLayout(self.newwidget)
        self.setCentralWidget(QtGui.QWidget(self))
        self.centralWidget().setLayout(self.mainLayout)
        self.showMaximized()
        self.eline = QtGui.QLineEdit()
        self.eline.setPlaceholderText( ("Username/Email/Mobile") )
        self.eline.setFixedWidth(250)
        self.passline = QtGui.QLineEdit()
        self.passline.setPlaceholderText( ("Enter Password") )
        self.passline.setFixedWidth(250)
        self.passline.setEchoMode(QtGui.QLineEdit.Password)
        self.loginlabel = QtGui.QLabel("Login")

        self.loginlabel.setStyleSheet("QLabel {font-size: 30px;}")
        self.loginBtn = QtGui.QPushButton("Sign in")
        self.loginBtn.clicked.connect(self.showDock)
        self.loginBtn.setStyleSheet("QPushButton{ background-color: #2a1b82; color: white; border: none; width: 5;height:30;}")
        self.hbox = QtGui.QHBoxLayout()
        self.imagelabel = QtGui.QLabel()
        self.imagelabel.resize(2200,1060)
        pixmap = QtGui.QPixmap("./frontimage1.png").scaled(self.imagelabel.size(), QtCore.Qt.KeepAspectRatio,QtCore.Qt.SmoothTransformation)
        self.imagelabel.setPixmap(pixmap)
        self.hbox.addWidget(self.imagelabel)
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.loginlabel)
        self.vbox.addWidget(self.eline)
        self.vbox.addWidget(self.passline)
        self.vbox.addWidget(self.loginBtn)
        self.vbox.setAlignment(QtCore.Qt.AlignCenter)
        self.mainLayout.addLayout(self.hbox)
        self.mainLayout.addLayout(self.vbox)
    def showDock(self):
        text = self.eline.text()
        text1 = self.passline.text()
        if text == "123"  and text1 == "123":
            self.mainProgram()
        elif text == "" or text1 == "" or text != "123" or text1 != "123":
            self.msg = QtGui.QMessageBox()
            self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
            self.msg.setText( ("Please Enter Valid Email") )
            self.msg.setWindowTitle( ("Email Not Valid") )
            self.msg.setStandardButtons(QtGui.QMessageBox.Retry)
            self.msg.exec_()
        else:
            pass
    def mainProgram(self):
        self.dialog = Table_Program(self)
        self.dialog.showFullScreen()
if __name__ == '__main__':
    import sys
    global settingobj
    app = QtGui.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    settingobj= Settings()
    settingobj.show()
    settingobj.showFullScreen()
    sys.exit(app.exec_())
